package com.agileai.miscdp.hotweb.ui.editors.simplest;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.domain.simplest.SimplestFuncModel;
import com.agileai.miscdp.ui.TextCheckPolicy;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.common.IniReader;
import com.agileai.domain.KeyNamePair;
import com.agileai.util.StringUtil;

/**
 * 最简模型编辑容器
 */
public class SimplestModelPanel extends Composite {
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Combo templateCombo;
	private Text pageNameText;
	private Text pageTitleText;
	private Text handlerClassText;
	private Text handlerIdText;
	private Text funcTypeText;
	private Text funcNameText;
	private SimplestModelEditor simplestEditorPart; 
	private SimplestFuncModel simplestFuncModel;
	private Text funcSubPkgText;
	
	private String mainPkg = null;
	private String funcSubPkg = "";
	private String handlerId = "MySample";
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public SimplestModelPanel(Composite parent,SimplestModelEditor editorPart, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.simplestEditorPart = editorPart;
		
		simplestFuncModel = (SimplestFuncModel)simplestEditorPart.getFuncModel();
		ProjectConfig projectConfig = new ProjectConfig();
		String projectName = simplestFuncModel.getProjectName();
		String configFile = MiscdpUtil.getCfgFileName(projectName,DeveloperConst.PROJECT_CFG_NAME);
		projectConfig.setConfigFile(configFile);
		projectConfig.initConfig();
		this.mainPkg = projectConfig.getMainPkg();
		
		
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		toolkit.adapt(funcNameText, true, true);

		final Label label_1_1 = new Label(this, SWT.NONE);
		label_1_1.setText("功能类型");
		
		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		toolkit.adapt(funcTypeText, true, true);
		
		Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER | SWT.READ_ONLY);
		funcSubPkgText.setEditable(true);
		new TextCheckPolicy(funcSubPkgText, TextCheckPolicy.POLICY_PACKAGE_NAME);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(funcSubPkgText, true, true);
		funcSubPkgText.setForeground(ResourceUtil.getColor(255, 255, 255));
		funcSubPkgText.setBackground(ResourceUtil.getColor(128, 0, 255));
		
		final Label handleridLabel = new Label(this, SWT.NONE);
		handleridLabel.setText("HandlerId");

		handlerIdText = new Text(this, SWT.BORDER);
		toolkit.adapt(handlerIdText, true, true);
		handlerIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		handlerIdText.setForeground(ResourceUtil.getColor(255, 255, 255));
		handlerIdText.setBackground(ResourceUtil.getColor(128, 0, 255));
		new TextCheckPolicy(handlerIdText, TextCheckPolicy.POLICY_MODEL_NAME);

		final Label pageTitleLabel = new Label(this, SWT.NONE);
		pageTitleLabel.setText("页面标题");

		pageTitleText = new Text(this, SWT.BORDER);
		toolkit.adapt(pageTitleText, true, true);
		pageTitleText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		final Label handlerClassLabel = new Label(this, SWT.NONE);
		handlerClassLabel.setText("页面处理类");

		handlerClassText = new Text(this, SWT.BORDER);
		handlerClassText.setEditable(false);
		toolkit.adapt(handlerClassText, true, true);
		handlerClassText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label pageNameLabel = new Label(this, SWT.NONE);
		pageNameLabel.setText("页面名称");

		pageNameText = new Text(this, SWT.BORDER);
		pageNameText.setEditable(false);
		toolkit.adapt(pageNameText, true, true);
		pageNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label funcTemplateLabel = new Label(this, SWT.NONE);
		funcTemplateLabel.setText("功能模板");

		templateCombo = new Combo(this, SWT.NONE);
		toolkit.adapt(templateCombo, true, true);
		templateCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		initValues();
		registryModifier();
	}
	
	private void changeRelatePkgPath(){
		String jspName = funcSubPkg +"/"+handlerId+".jsp";
		pageNameText.setText(jspName);
		String handlerIdClass = mainPkg+".controller."+funcSubPkg+"."+handlerId+"Handler";
		handlerClassText.setText(handlerIdClass);
	}

	@SuppressWarnings("rawtypes")
	private void initValues(){
		if (StringUtil.isNullOrEmpty(simplestFuncModel.getHandlerId())){
			simplestFuncModel.setHandlerId(handlerId);
			simplestFuncModel.setFuncSubPkg(funcSubPkg);
			simplestFuncModel.setDefaultJspName(funcSubPkg +"/"+handlerId+".jsp");
			simplestFuncModel.setHandlerClass(mainPkg+".controller."+funcSubPkg+"."+handlerId+"Handler");	
		}
		
		funcNameText.setText(simplestFuncModel.getFuncName());
		funcTypeText.setText(simplestFuncModel.getFuncTypeName());
		funcSubPkgText.setText(simplestFuncModel.getFuncSubPkg());
		handlerIdText.setText(simplestFuncModel.getHandlerId());
		pageTitleText.setText(simplestFuncModel.getDefaultJspTitle());
		handlerClassText.setText(simplestFuncModel.getHandlerClass());
		pageNameText.setText(simplestFuncModel.getDefaultJspName());
		
		IniReader reader = MiscdpUtil.getIniReader();
		List defValueList = reader.getList("SimplestModelTemplates");
		int selectedIndex = 0;
		for (int i=0;i < defValueList.size();i++){
			KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
			String key = keyNamePair.getKey();
			if (simplestFuncModel.getTemplate() != null && simplestFuncModel.getTemplate().equals(key)){
				selectedIndex = i;
			}
			templateCombo.add(key);
		}
		templateCombo.select(selectedIndex);
	}
	
	private void registryModifier(){
		
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				simplestEditorPart.setModified(true);
				simplestEditorPart.fireDirty();
			}
		});
		
		funcSubPkgText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				funcSubPkg = funcSubPkgText.getText();
				if (!funcSubPkg.equals(simplestFuncModel.getFuncSubPkg())){
					changeRelatePkgPath();
					simplestEditorPart.setModified(true);
					simplestEditorPart.fireDirty();					
				}
			}
		});
		
		handlerIdText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				handlerId = handlerIdText.getText();
				if (!handlerId.equals(simplestFuncModel.getHandlerId())){
					changeRelatePkgPath();
					simplestEditorPart.setModified(true);
					simplestEditorPart.fireDirty();					
				}
			}
		});
		
		pageTitleText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				simplestEditorPart.setModified(true);
				simplestEditorPart.fireDirty();
			}
		});
		
		handlerClassText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				simplestEditorPart.setModified(true);
				simplestEditorPart.fireDirty();
			}
		});
		
		pageNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				simplestEditorPart.setModified(true);
				simplestEditorPart.fireDirty();
			}
		});
		
		templateCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				simplestEditorPart.setModified(true);
				simplestEditorPart.fireDirty();
			}
		});
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Text getFuncTypeText() {
		return funcTypeText;
	}

	public Text getFuncNameText() {
		return funcNameText;
	}

	public Combo getTemplateCombo() {
		return templateCombo;
	}

	public Text getPageNameText() {
		return pageNameText;
	}

	public Text getPageTitleText() {
		return pageTitleText;
	}
	
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}
	
	public Text getHandlerClassText() {
		return handlerClassText;
	}

	public Text getHandlerIdText() {
		return handlerIdText;
	}
}
