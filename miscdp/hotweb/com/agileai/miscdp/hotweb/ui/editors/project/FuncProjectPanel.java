package com.agileai.miscdp.hotweb.ui.editors.project;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;

import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.util.MiscdpUtil;

/**
 * 功能工程编辑器容器
 */
public class FuncProjectPanel extends Composite {
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Text projectNameText;
	private FuncProjectEditor projectEditorPart; 
	private Label label_1;
	private Text serverAddressText;
	private Label label_2;
	private Text serverPortText;
	private Label projectTypeLabel;
	private Text projectTypeText;

	public FuncProjectPanel(Composite parent, int style,FuncProjectEditor editorPart) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.projectEditorPart = editorPart;
		
		this.createContent();
		this.initValues();
		this.registryModifier();
	}
	
	protected void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);
		
		projectTypeLabel = new Label(this, SWT.NONE);
		projectTypeLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		projectTypeLabel.setText("应用类型");
		
		projectTypeText = new Text(this, SWT.BORDER);
		projectTypeText.setEditable(false);
		projectTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(projectTypeText, true, true);

		final Label label = new Label(this, SWT.NONE);
		label.setText("应用名称");

		projectNameText = new Text(this, SWT.BORDER);
		projectNameText.setText(projectEditorPart.getFuncProject().getProjectName());
		projectNameText.setEditable(false);
		projectNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		toolkit.adapt(projectNameText, true, true);
		
		label_1 = new Label(this, SWT.NONE);
		label_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_1.setText("服务器地址");
		
		serverAddressText = new Text(this, SWT.BORDER);
		serverAddressText.setEditable(false);
		serverAddressText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		serverAddressText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(serverAddressText, true, true);
		
		label_2 = new Label(this, SWT.NONE);
		label_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_2.setText("服务器端口");
		
		serverPortText = new Text(this, SWT.BORDER);
		serverPortText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(serverPortText, true, true);
		new Label(this, SWT.NONE);
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}
	private void initValues(){
		String projectName = projectEditorPart.getFuncProject().getProjectName();
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(projectName);
		String serverPort = projectConfig.getServerPort();
		String serverAddress = projectConfig.getServerAddress();
		
		serverPortText.setText(serverPort);
		serverAddressText.setText(serverAddress);
		if (projectConfig.isIntegrateWebProject()){
			projectTypeText.setText("集成Web项目");
		}else{
			projectTypeText.setText("普通Web项目");
		}
	}
	
	private void registryModifier(){
		serverPortText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Text getServerPortText(){
		return serverPortText;
	}
}
