﻿package com.agileai.miscdp.hotweb.ui.editors.query;

import java.io.File;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;

import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.PageParameter;
import com.agileai.miscdp.hotweb.domain.query.QueryFuncModel;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;
import com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.hotweb.model.EditTableType;
import com.agileai.hotweb.model.ListTableType;
import com.agileai.hotweb.model.QueryBarType;
import com.agileai.hotweb.model.query.QueryFuncModelDocument;
import com.agileai.hotweb.model.query.QueryFuncModelDocument.QueryFuncModel.BaseInfo;
import com.agileai.hotweb.model.query.QueryFuncModelDocument.QueryFuncModel.BaseInfo.Service;
import com.agileai.hotweb.model.query.QueryFuncModelDocument.QueryFuncModel.BaseInfo.SqlResource;
import com.agileai.hotweb.model.query.QueryFuncModelDocument.QueryFuncModel.DetailView;
import com.agileai.hotweb.model.query.QueryFuncModelDocument.QueryFuncModel.ListView;
import com.agileai.util.FileUtil;
import com.agileai.util.StringUtil;
/**
 * 单表操作功能模型编辑器
 */
public class QueryModelEditor extends BaseModelEditor{
	private QMBasicConfigPage qMBasicConfigPage;
	private QMListJspCfgPage qMListJspCfgPage;
	private QMDetailJspCfgPage qMDetailJspCfgPage;
	
    public void init(IEditorSite site, IEditorInput input) throws PartInitException {
        setSite(site);
        setInput(input);
        setPartName(input.getName());
		init();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
    }
	private void init() {
		funcModel = (QueryFuncModel)this.getEditorInput();
	}

	protected void createPages() {
		qMBasicConfigPage = new QMBasicConfigPage(getContainer(),this,SWT.NONE);
		qMListJspCfgPage = new QMListJspCfgPage(getContainer(),this,SWT.NONE);
		qMDetailJspCfgPage = new QMDetailJspCfgPage(getContainer(),this,SWT.NONE);
		setFuncModel((QueryFuncModel)funcModel);
		
		String basicConfig = MiscdpUtil.getIniReader().getValue("EditorInfo","BasicInfo");
		int basicConfigIndex = addPage(qMBasicConfigPage);
		setPageText(basicConfigIndex,basicConfig);
		qMBasicConfigPage.createContent();
		
		String listJspConfig = MiscdpUtil.getIniReader().getValue("EditorInfo","ListInfo");
		int listJspCfgIndex = addPage(qMListJspCfgPage);
		setPageText(listJspCfgIndex,listJspConfig);
		qMListJspCfgPage.createContent();
		
		String detailJspConfig = MiscdpUtil.getIniReader().getValue("EditorInfo","DetailInfo");
		int detailJspCfgIndex = addPage(qMDetailJspCfgPage);
		setPageText(detailJspCfgIndex, detailJspConfig);
		qMDetailJspCfgPage.createContent();
		
		initValues();
		
		qMBasicConfigPage.registryModifier();
		qMListJspCfgPage.registryModifier();
		qMDetailJspCfgPage.registryModifier();
	}
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}
	public void doSave(IProgressMonitor monitor) {
		try {
			QueryFuncModel queryFuncModel = buildFuncModel();
	    	String projectName = queryFuncModel.getProjectName();
	    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
	    	
	    	String listJspName = qMBasicConfigPage.getQMExtendAttribute().getListJspNameText().getText();
	    	if (StringUtil.isNullOrEmpty(listJspName)){
	    		String listJspCheckNullMsg = MiscdpUtil.getIniReader().getValue("QueryModelEditor","ListJspCheckNullMsg");
	    		DialogUtil.showInfoMessage(listJspCheckNullMsg);
	    		return;
	    	}
	    	if (queryFuncModel.isShowDetail()){
		    	String recordIds = qMDetailJspCfgPage.getRecordIdsText().getText();
		    	if (StringUtil.isNullOrEmpty(recordIds)){
		    		String recordIdsCheckNullMsg = MiscdpUtil.getIniReader().getValue("QueryModelEditor","RecordIdsCheckNullMsg");
		    		DialogUtil.showInfoMessage(recordIdsCheckNullMsg);
		    		return;
		    	}	    		
	    	}
	    	
	    	QueryFuncModelDocument funcModelDocument = (QueryFuncModelDocument)buildFuncModelDocument(queryFuncModel);
			StringWriter writer = new StringWriter();
			funcModelDocument.save(writer);
			String funcName = queryFuncModel.getFuncName();
			String funcSubPkg = queryFuncModel.getFuncSubPkg();
			
			String funcDefine = writer.toString();
			String funcId = queryFuncModel.getFuncId();
			String fileName = MiscdpUtil.getFuncFileName(projectName,funcId);
			FileUtil.writeFile(fileName, funcDefine);
			
			this.isModified = false;
			firePropertyChange(PROP_DIRTY);
			
			TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
			ISelection selection = funcTreeViewer.getSelection();
			Object obj = ((IStructuredSelection) selection).getFirstElement();
			TreeObject treeObject = null;
			if (obj instanceof TreeObject) {
				treeObject = (TreeObject) obj;
				treeObject.setName(funcName);
				treeObject.setFuncSubPkg(funcSubPkg);
			}
			
			updateIndexFile(projectName);
			
			this.setPartName(funcName);
			funcTreeViewer.refresh();
			try {
				queryFuncModel.buildFuncModel(funcDefine);
				project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void doSaveAs() {
	}
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}
	public boolean isDirty(){
		return this.isModified;
	}
	public void setActivePage(int pageIndex) {
		super.setActivePage(pageIndex);
	}
	public boolean isSaveAsAllowed() {
		return false;
	}
	public void fireDirty(){
		firePropertyChange(PROP_DIRTY);
	}
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);
	}
	public void resourceChanged(final IResourceChangeEvent event){
	}
	public QMBasicConfigPage getBasicConfigPage() {
		return qMBasicConfigPage;
	}
	public QMListJspCfgPage getListJspCfgPage() {
		return qMListJspCfgPage;
	}
	public QMDetailJspCfgPage getDetailJspCfgPage() {
		return qMDetailJspCfgPage;
	}
	public void setFuncModel(QueryFuncModel suFuncModel) {
		this.qMBasicConfigPage.setFuncModel(suFuncModel);
		this.qMListJspCfgPage.setFuncModel(suFuncModel);
		this.qMDetailJspCfgPage.setFuncModel(suFuncModel);
	}
	public QueryFuncModel buildFuncModel() {
		this.qMBasicConfigPage.buildConfig();
		this.qMListJspCfgPage.buildConfig();
		return this.qMDetailJspCfgPage.buildConfig();
	}
	public void initValues() {
		this.qMBasicConfigPage.initValues();
		this.qMListJspCfgPage.initValues();
		this.qMDetailJspCfgPage.initValues();
	}
	public QueryFuncModel getFuncModel() {
		return (QueryFuncModel)funcModel;
	}
	public void setFuncModelTree(FuncModelTreeView funcModelTree) {
		this.funcModelTree = funcModelTree;
	}
	public void setModified(boolean isModified) {
		this.isModified = isModified;
	}
	public Object buildFuncModelDocument(BaseFuncModel baseFuncModel) {
		QueryFuncModel queryFuncModel = (QueryFuncModel)baseFuncModel;
		QueryFuncModelDocument funcModelDocument = QueryFuncModelDocument.Factory.newInstance();
		QueryFuncModelDocument.QueryFuncModel funcModel = funcModelDocument.addNewQueryFuncModel();
		BaseInfo baseInfo = funcModel.addNewBaseInfo();
		String detailJspName = queryFuncModel.getDetailJspName();
		String listJspName = queryFuncModel.getListJspName();
		
		baseInfo.setDetailJspName(detailJspName);
		baseInfo.setDetailTitle(queryFuncModel.getDetailTitle());
		baseInfo.setTemplate(queryFuncModel.getTemplate());
		baseInfo.setListJspName(listJspName);
		baseInfo.setListTitle(queryFuncModel.getListTitle());
		baseInfo.setQueryListSql(queryFuncModel.getListSql());
		baseInfo.setShowDetail(queryFuncModel.isShowDetail());
		
		Service service = baseInfo.addNewService();
		String interfaceName = queryFuncModel.getInterfaceName();
		service.setServiceId(queryFuncModel.getServiceId());
		service.setImplClassName(queryFuncModel.getImplClassName());
		service.setInterfaceName(interfaceName);
		
		String editHandlerId = detailJspName.substring(0,detailJspName.length()-4);
		editHandlerId = editHandlerId.substring(editHandlerId.lastIndexOf("/")+1,editHandlerId.length());
		String listHandlerId = listJspName.substring(0,listJspName.length()-4);
		listHandlerId = listHandlerId.substring(listHandlerId.lastIndexOf("/")+1,listHandlerId.length());
		String tempClassPath = interfaceName.substring(0,interfaceName.lastIndexOf(".")).replace("bizmoduler","controller"); 
		String listHandlerClass = tempClassPath + "." + listHandlerId + "Handler";
		String editHandlerClass = tempClassPath + "." + editHandlerId + "Handler";
		BaseInfo.Handler handler = baseInfo.addNewHandler();
		handler.setDetailHandlerId(editHandlerId);
		handler.setDetailHandlerClass(editHandlerClass);
		handler.setListHandlerId(listHandlerId);
		handler.setListHandlerClass(listHandlerClass);
		
		String sqlMapFile = queryFuncModel.getSqlMapFile();
		if (StringUtil.isNullOrEmpty(sqlMapFile)){
			DialogUtil.showInfoMessage("SqlMap文件不能为空！");
			return null;
		}
		File tempSqlMapFile = new File(sqlMapFile);
		String sqlNameSpace = tempSqlMapFile.getName().substring(0,tempSqlMapFile.getName().lastIndexOf("."));
		SqlResource sqlResource = baseInfo.addNewSqlResource();
		sqlResource.setSqlNameSpace(sqlNameSpace);
		
		String tempSqlMapFilePath = sqlMapFile.replace("\\", "/");
		sqlResource.setSqlMapPath(tempSqlMapFilePath);
		
		ListView listView = funcModel.addNewListView();
		List<PageParameter> pageParameters = queryFuncModel.getPageParameters();
		
		QueryBarType queryBarType = listView.addNewParameterArea(); 
		this.processQueryBarType(queryBarType, pageParameters);
		
		ListTableType listTableType = listView.addNewListTableArea();
		listTableType.setExportCsv(queryFuncModel.isExportCsv());
		listTableType.setExportXls(queryFuncModel.isExportXls());
		listTableType.setPagination(queryFuncModel.isPagination());
		listTableType.setSortAble(queryFuncModel.isSortAble());
		
		List<ListTabColumn> listTabColumns = queryFuncModel.getListTableColumns();
		String rsIdColumn = StringUtils.join(queryFuncModel.getRsIdColumns().toArray(),",");
		this.processListTableType(listTableType, listTabColumns, rsIdColumn);
		
		DetailView detailView = funcModel.addNewDetailView();
		EditTableType editTableType = detailView.addNewDetailViewArea();
		List<PageFormField> pageFormFields = queryFuncModel.getPageFormFields();
		this.processEditTableType(editTableType, pageFormFields);
		
		return funcModelDocument;
	}
	public QMBasicConfigPage getQMBasicConfigPage() {
		return qMBasicConfigPage;
	}
	public QMListJspCfgPage getQMListJspCfgPage() {
		return qMListJspCfgPage;
	}
	public QMDetailJspCfgPage getQMDetailJspCfgPage() {
		return qMDetailJspCfgPage;
	}
}
