package com.agileai.miscdp.hotweb.ui.editors.query;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.agileai.domain.KeyNamePair;
/**
 * 记录标识选择框
 */
public class RecordIdsDialog extends Dialog {
	boolean cancelEdit = true;
	private List<KeyNamePair> selectedKeyNamePairList = new ArrayList<KeyNamePair>();
	private List<KeyNamePair> inputKeyNamePairList = new ArrayList<KeyNamePair>();
	private List<KeyNamePair> midStateKeyNamePairList = new ArrayList<KeyNamePair>();
	private CheckboxTableViewer checkboxTableViewer;
	public RecordIdsDialog(Shell parentShell) {
		super(parentShell);
	}

	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final Label label = new Label(container, SWT.NONE);
		label.setText("请能够唯一确定记录的字段：");
		
		Table table = new Table(container,SWT.MULTI | SWT.BORDER | SWT.CHECK |SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				midStateKeyNamePairList.clear();
				Object[] objects = checkboxTableViewer.getCheckedElements();
				if (objects != null){
					for (int i=0;i < objects.length;i++){
						Object object = objects[i];	
						midStateKeyNamePairList.add((KeyNamePair)object);
					}
				}
			}
		});
		checkboxTableViewer = new CheckboxTableViewer(table);
		
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final TableColumn newColumnTableColumn = new TableColumn(table, SWT.NONE);
		newColumnTableColumn.setWidth(206);
		newColumnTableColumn.setText("可选字段");
		checkboxTableViewer.setLabelProvider(new RecordIds());
		checkboxTableViewer.setContentProvider(new ArrayContentProvider());
		
		checkboxTableViewer.setInput(inputKeyNamePairList);
		checkboxTableViewer.setCheckedElements(selectedKeyNamePairList.toArray());
		return container;
	}
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	protected Point getInitialSize() {
		return new Point(270, 320);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("记录标识选择框");
	}
	public List<KeyNamePair> getSelectedKeyNamePairs() {
		List<KeyNamePair> list = new ArrayList<KeyNamePair>();
		for (int i=0;i < selectedKeyNamePairList.size();i++){
			KeyNamePair keyNamePair = (KeyNamePair)selectedKeyNamePairList.get(i);
			list.add(keyNamePair);
		}
		return list;
	}
	public void addSelectedKeyNamePair(KeyNamePair keyNamePair) {
		this.selectedKeyNamePairList.add(keyNamePair);
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			cancelEdit = false;
			if (midStateKeyNamePairList.size() > 0){
				this.selectedKeyNamePairList.clear();
				for (int i=0;i < midStateKeyNamePairList.size();i++){
					this.selectedKeyNamePairList.add(this.midStateKeyNamePairList.get(i));
				}
			}
		}
		super.buttonPressed(buttonId);
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setInputKeyNamePairList(List inputKeyNamePairList) {
		this.inputKeyNamePairList = inputKeyNamePairList;
	}
	public boolean isCancelEdit() {
		return cancelEdit;
	}
}
class RecordIds extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof KeyNamePair) {
			KeyNamePair p = (KeyNamePair) element;
			if (columnIndex == 0) {
				return p.getKey();
			} else if (columnIndex == 1) {
				return p.getValue();
			}
		}
		return null;
	}
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
