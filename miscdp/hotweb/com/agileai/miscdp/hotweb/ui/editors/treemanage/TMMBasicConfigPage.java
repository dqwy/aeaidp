package com.agileai.miscdp.hotweb.ui.editors.treemanage;

import java.util.List;

import net.sf.swtaddons.autocomplete.combo.AutocompleteComboInput;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.treemanage.TreeManageFuncModel;
import com.agileai.miscdp.hotweb.ui.dialogs.FieldSelectDialog;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
/**
 * 基础配置页面
 */
public class TMMBasicConfigPage extends Composite implements Modifier {
	private Text idFieldText;
	private Text nameFieldText;
	private Text sortFieldText;
	private Text pidFieldText;
	private Text funcNameText;
	private Text funcTypeText;
	private TreeManageModelEditor modelEditor = null;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private TMMExtendAttribute extendAttributes;
	private Combo tableNameCombo;

	private TreeManageFuncModel funcModel= null;
	private Text funcSubPkgText;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setTableComboValues(String schema) {
		String projectName = funcModel.getProjectName();
		List tableList = DBManager.getInstance(projectName).getTables(schema);
		String[] tables = (String[]) tableList.toArray(new String[0]);
		this.tableNameCombo.setItems(tables);
	}
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public TMMBasicConfigPage(Composite parent,TreeManageModelEditor suModelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditor = suModelEditor;
		this.funcModel = suModelEditor.getFuncModel();
//		createContent();
	}
	public void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		toolkit.adapt(funcNameText, true, true);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能类型");

		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		toolkit.adapt(funcTypeText, true, true);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		Label label_3 = new Label(this, SWT.NONE);
		label_3.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(funcSubPkgText, true, true);

		final Label tablenameLabel = new Label(this, SWT.NONE);
		tablenameLabel.setText("数据表");

		tableNameCombo = new Combo(this, SWT.NONE);
		tableNameCombo.setForeground(ResourceUtil.getColor(255, 255, 255));
		tableNameCombo.setBackground(ResourceUtil.getColor(128, 0, 255));
		tableNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		new AutocompleteComboInput(tableNameCombo);
		
		final Composite formComposite = new Composite(this, SWT.NONE);
		formComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.marginHeight = 2;
		gridLayout_4.horizontalSpacing = 0;
		gridLayout_4.verticalSpacing = 0;
		gridLayout_4.marginWidth = 0;
		formComposite.setLayout(gridLayout_4);

		final TabFolder tabFolder = new TabFolder(formComposite, SWT.NONE);
		final GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_tabFolder.heightHint = 254;
		tabFolder.setLayoutData(gd_tabFolder);
		toolkit.adapt(tabFolder, true, true);
		
		final TabItem tabItem_1 = new TabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("典型字段");

		final Composite composite = new Composite(tabFolder, SWT.NONE);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 3;
		composite.setLayout(gridLayout_1);
		toolkit.adapt(composite);
		tabItem_1.setControl(composite);

		final Label label_2 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2, true, true);
		label_2.setText("节点ID字段");
		
		idFieldText = new Text(composite, SWT.BORDER);
		toolkit.adapt(idFieldText, true, true);
		idFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		idFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(idFieldText);
			}
		});
		new Label(composite, SWT.NONE).setText("*");
		
		final Label label_2_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1, true, true);
		label_2_1.setText("节点名称字段");

		nameFieldText = new Text(composite, SWT.BORDER);
		toolkit.adapt(nameFieldText, true, true);
		nameFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		nameFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(nameFieldText);
			}
		});
		new Label(composite, SWT.NONE).setText("*");
		
		final Label label_2_1_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1_1, true, true);
		label_2_1_1.setText("父节点ID字段");

		pidFieldText = new Text(composite, SWT.BORDER);
		toolkit.adapt(pidFieldText, true, true);
		pidFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		pidFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(pidFieldText);
			}
		});
		new Label(composite, SWT.NONE).setText("*");
		
		final Label label_2_1_1_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1_1_1, true, true);
		label_2_1_1_1.setText("节点排序字段");

		sortFieldText = new Text(composite, SWT.BORDER);
		toolkit.adapt(sortFieldText, true, true);
		sortFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		sortFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(sortFieldText);
			}
		});
		new Label(composite, SWT.NONE).setText("*");

		final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("扩展属性");
		extendAttributes = new TMMExtendAttribute(tabFolder,SWT.NONE);
		tabItem.setControl(extendAttributes);
		setTableComboValues(null);
	}
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}
	public Combo getTableNameCombo() {
		return tableNameCombo;
	}
	private void openFieldSelectDialog(Text targetText){
		String tableName = tableNameCombo.getText();
		String projectName = funcModel.getProjectName();
		String[] fields = MiscdpUtil.getColNames(DBManager.getInstance(projectName), tableName);
		FieldSelectDialog dialog = new FieldSelectDialog(modelEditor,fields,targetText);
		dialog.open();
	}
	public TreeManageFuncModel buildConfig(){
		int tableIndex = tableNameCombo.getSelectionIndex();
		if (tableIndex != -1) {
			String tableName = tableNameCombo.getItem(tableIndex);
			funcModel.setTableName(tableName);
		}
		funcModel.setFuncName(funcNameText.getText());
//		funcModel.setFuncType(funcTypeText.getText());
		funcModel.setFuncSubPkg(funcSubPkgText.getText());
		
		funcModel.setIdField(idFieldText.getText());
		funcModel.setNameField(nameFieldText.getText());
		funcModel.setSortField(sortFieldText.getText());
		funcModel.setParentIdField(pidFieldText.getText());
		
		funcModel.setServiceId(extendAttributes.getServiceIdText().getText());
		funcModel.setImplClassName(extendAttributes.getImplClassNameText().getText());
		funcModel.setInterfaceName(extendAttributes.getInterfaceNameText().getText());
		funcModel.setJspName(extendAttributes.getJspNameText().getText());

		funcModel.setTemplate(extendAttributes.getTemplateCombo().getItem(0));
		return this.funcModel;
	}

	public void initValues() {
		int tableCount = this.tableNameCombo.getItemCount();
		if (funcModel.getTableName() != null){
			for (int i=0;i < tableCount;i++){
				String tableName = this.tableNameCombo.getItem(i).toLowerCase();
				if (this.funcModel.getTableName().toLowerCase().equals(tableName)){
					this.tableNameCombo.select(i);
					break;
				}
			}
		}
		idFieldText.setText(this.funcModel.getIdField());
		nameFieldText.setText(this.funcModel.getNameField());
		pidFieldText.setText(this.funcModel.getParentIdField());
		sortFieldText.setText(this.funcModel.getSortField());
		
		funcNameText.setText(this.funcModel.getFuncName());
		funcSubPkgText.setText(this.funcModel.getFuncSubPkg());
		funcTypeText.setText(this.funcModel.getFuncTypeName());
		extendAttributes.getServiceIdText().setText(funcModel.getServiceId());
		extendAttributes.getImplClassNameText().setText(funcModel.getImplClassName());
		extendAttributes.getInterfaceNameText().setText(funcModel.getInterfaceName());
		extendAttributes.getJspNameText().setText(funcModel.getJspName());
		extendAttributes.getTemplateCombo().select(0);
	}
	public void registryModifier() {
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		funcSubPkgText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		
		sortFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		idFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		nameFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pidFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		
		extendAttributes.getTemplateCombo().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttributes.getJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttributes.getInterfaceNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttributes.getImplClassNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttributes.getServiceIdText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		tableNameCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
	public Text getIdFieldText() {
		return idFieldText;
	}
	public void setIdFieldText(Text idFieldText) {
		this.idFieldText = idFieldText;
	}
	public Text getNameFieldText() {
		return nameFieldText;
	}
	public void setNameFieldText(Text nameFieldText) {
		this.nameFieldText = nameFieldText;
	}
	public Text getPidFieldText() {
		return pidFieldText;
	}
	public void setPidFieldText(Text pidFieldText) {
		this.pidFieldText = pidFieldText;
	}
	public Text getSortFieldText() {
		return sortFieldText;
	}
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}
	public void setSortFieldText(Text sortFieldText) {
		this.sortFieldText = sortFieldText;
	}
	public TMMExtendAttribute getExtendAttributes() {
		return extendAttributes;
	}
}
