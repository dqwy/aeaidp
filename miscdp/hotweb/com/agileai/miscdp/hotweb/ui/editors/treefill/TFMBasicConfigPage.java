package com.agileai.miscdp.hotweb.ui.editors.treefill;

import java.io.File;

import org.apache.commons.lang.BooleanUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.database.SqlBuilder;
import com.agileai.miscdp.hotweb.domain.treefill.TreeFillFuncModel;
import com.agileai.miscdp.hotweb.ui.dialogs.FieldSelectDialog;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.ui.TextCheckPolicy;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.util.StringUtil;
/**
 * 基础配置页面
 */
public class TFMBasicConfigPage extends Composite implements Modifier {
	private Text rootIdParamKeyText;
	private Text nodePIdFieldText;
	private Text nodeNameFieldText;
	private Text nodeIdFieldText;
	private Combo isMultiCombo;
	private Text sqlMapFileText;
	private TreeFillModelEditor modelEditor = null;
	private Text funcNameText;
	private Text funcTypeText;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private TFMExtendAttribute pfMExtendAttribute;
	private Text listSqlText;
	private TreeFillFuncModel funcModel= null;
	private Text funcSubPkgText;
	
	public TFMBasicConfigPage(Composite parent,TreeFillModelEditor suModelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditor = suModelEditor;
//		createContent();
	}
	public void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 3;
		setLayout(gridLayout);

		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		toolkit.adapt(funcNameText, true, true);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能类型");

		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		toolkit.adapt(funcTypeText, true, true);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);
		
		Label label_3 = new Label(this, SWT.NONE);
		label_3.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER );
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new TextCheckPolicy(funcSubPkgText,TextCheckPolicy.POLICY_PACKAGE_NAME);
		toolkit.adapt(funcSubPkgText, true, true);
		new Label(this, SWT.NONE);

		final Label targetPackageLabel_1_1 = new Label(this, SWT.NONE);
		targetPackageLabel_1_1.setText("SqlMap文件");

		sqlMapFileText = toolkit.createText(this, null);
		sqlMapFileText.setForeground(ResourceUtil.getColor(255, 255, 255));
		sqlMapFileText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		sqlMapFileText.setBackground(ResourceUtil.getColor(128, 0, 255));
		
		final Button button = new Button(this, SWT.NONE);
		GridData gd_button = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_button.heightHint = 23;
		button.setLayoutData(gd_button);
		toolkit.adapt(button, true, true);
		button.setText("选择");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Shell shell = new Shell();
				FileDialog fd = new FileDialog(shell,SWT.OPEN);
				fd.setText("Open");
				String projectName = funcModel.getProjectName();
				fd.setFilterPath(MiscdpUtil.getDefaultSqlMapPath(projectName));	
				
				String[] extensions = {"*.xml"};
				fd.setFilterExtensions(extensions);
				String selectedFile = fd.open();
				if (selectedFile != null){
					String sqlMapFilePath = selectedFile.substring(0,selectedFile.lastIndexOf(File.separator)+1)+"SomeTreeSelect.xml";
					sqlMapFileText.setText(sqlMapFilePath);
				}
			}
		});

		final Label targetPackageLabel_1_1_1 = new Label(this, SWT.NONE);
		targetPackageLabel_1_1_1.setText("是否多选");

		isMultiCombo = new Combo(this, SWT.NONE);
		isMultiCombo.add("Yes");
		isMultiCombo.add("No");
		toolkit.adapt(isMultiCombo, true, true);
		isMultiCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);

		final Label label_2 = new Label(this, SWT.NONE);
		label_2.setText("节点标识字段");

		nodeIdFieldText = new Text(this, SWT.BORDER);
		toolkit.adapt(nodeIdFieldText, true, true);
		nodeIdFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		nodeIdFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(nodeIdFieldText);
			}
		});
		new Label(this, SWT.NONE).setText("*");

		final Label label_2_1 = new Label(this, SWT.NONE);
		label_2_1.setText("节点名称字段");

		nodeNameFieldText = new Text(this, SWT.BORDER);
		toolkit.adapt(nodeNameFieldText, true, true);
		nodeNameFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		nodeNameFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(nodeNameFieldText);
			}
		});
		new Label(this, SWT.NONE).setText("*");

		final Label label_2_1_1 = new Label(this, SWT.NONE);
		label_2_1_1.setText("父节点标识字段");

		nodePIdFieldText = new Text(this, SWT.BORDER);
		toolkit.adapt(nodePIdFieldText, true, true);
		nodePIdFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		nodePIdFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(nodePIdFieldText);
			}
		});
		new Label(this, SWT.NONE).setText("*");;

		final Label label_2_1_1_1_1 = new Label(this, SWT.NONE);
		label_2_1_1_1_1.setText("根节点参数编码");

		rootIdParamKeyText = new Text(this, SWT.BORDER);
		toolkit.adapt(rootIdParamKeyText, true, true);
		rootIdParamKeyText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);
		
		final Composite formComposite = new Composite(this, SWT.NONE);
		formComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		formComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 3;
		gridLayout_4.marginHeight = 2;
		gridLayout_4.horizontalSpacing = 0;
		gridLayout_4.verticalSpacing = 0;
		gridLayout_4.marginWidth = 0;
		formComposite.setLayout(gridLayout_4);

		final TabFolder tabFolder = new TabFolder(formComposite, SWT.NONE);
		final GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 3);
		gd_tabFolder.widthHint = 494;
		gd_tabFolder.heightHint = 254;
		tabFolder.setLayoutData(gd_tabFolder);
		toolkit.adapt(tabFolder, true, true);
		
		final TabItem tabItem_1 = new TabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("列表SQL");

		listSqlText = new Text(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		toolkit.adapt(listSqlText, true, true);
		tabItem_1.setControl(listSqlText);

		final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("扩展属性");
		pfMExtendAttribute = new TFMExtendAttribute(tabFolder,SWT.NONE,this.modelEditor,listSqlText);
		tabItem.setControl(pfMExtendAttribute);
	}
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}
	public void setFuncModel(TreeFillFuncModel standardFuncModel) {
		this.funcModel = standardFuncModel;
	}
	public TreeFillFuncModel buildConfig(){
		funcModel.setFuncName(funcNameText.getText());
//		funcModel.setFuncType(funcTypeText.getText());
		funcModel.setFuncSubPkg(funcSubPkgText.getText());
		funcModel.setRootIdParamKey(rootIdParamKeyText.getText());
		funcModel.setNodePIdField(nodePIdFieldText.getText());
		funcModel.setNodeNameField(nodeNameFieldText.getText());
		funcModel.setNodeIdField(nodeIdFieldText.getText());
		String selectedValue = isMultiCombo.getItem(isMultiCombo.getSelectionIndex());
		funcModel.setIsMultiSelect(BooleanUtils.toBoolean(selectedValue));
		
		funcModel.setListSql(listSqlText.getText());
		funcModel.setSqlMapFile(sqlMapFileText.getText());
		funcModel.setServiceId(pfMExtendAttribute.getServiceIdText().getText());
		funcModel.setImplClassName(pfMExtendAttribute.getImplClassNameText().getText());
		funcModel.setInterfaceName(pfMExtendAttribute.getInterfaceNameText().getText());
		funcModel.setListJspName(pfMExtendAttribute.getListJspNameText().getText());
		
		funcModel.setExcludeIdParamKey(pfMExtendAttribute.getExcludeIdParamKeyText().getText());
		funcModel.setNodeTypeField(pfMExtendAttribute.getNodeTypeFieldText().getText());
		
		funcModel.setTemplate(pfMExtendAttribute.getTemplateCombo().getItem(0));
		return this.funcModel;
	}

	public void initValues() {
		funcNameText.setText(this.funcModel.getFuncName());
		funcTypeText.setText(this.funcModel.getFuncTypeName());
		funcSubPkgText.setText(this.funcModel.getFuncSubPkg());
		listSqlText.setText(this.funcModel.getListSql().trim());

		rootIdParamKeyText.setText(this.funcModel.getRootIdParamKey());
		nodePIdFieldText.setText(this.funcModel.getNodePIdField());
		nodeNameFieldText.setText(this.funcModel.getNodeNameField());
		nodeIdFieldText.setText(this.funcModel.getNodeIdField());
		if (this.funcModel.getIsMultiSelect()){
			isMultiCombo.select(0);	
		}else{
			isMultiCombo.select(1);
		}
		
		sqlMapFileText.setText(this.funcModel.getSqlMapFile());
		
		pfMExtendAttribute.getServiceIdText().setText(funcModel.getServiceId());
		pfMExtendAttribute.getImplClassNameText().setText(funcModel.getImplClassName());
		pfMExtendAttribute.getInterfaceNameText().setText(funcModel.getInterfaceName());
		pfMExtendAttribute.getListJspNameText().setText(funcModel.getListJspName());
		
		pfMExtendAttribute.getExcludeIdParamKeyText().setText(this.funcModel.getExcludeIdParamKey());
		pfMExtendAttribute.getNodeTypeFieldText().setText(this.funcModel.getNodeTypeField());
		
		pfMExtendAttribute.getTemplateCombo().select(0);
	}
	public Text getListSqlText() {
		return listSqlText;
	}
	public void registryModifier() {
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		funcSubPkgText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				if (!funcSubPkgText.getText().equals(funcModel.getFuncSubPkg())){
					modelEditor.setModified(true);
					modelEditor.fireDirty();				
				}
			}
		});
		
		rootIdParamKeyText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		nodePIdFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		nodeNameFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		nodeIdFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		isMultiCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		
		pfMExtendAttribute.getTemplateCombo().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getListJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getInterfaceNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getImplClassNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getExcludeIdParamKeyText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getNodeTypeFieldText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		
		pfMExtendAttribute.getServiceIdText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		listSqlText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		
		sqlMapFileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
	private void openFieldSelectDialog(Text targetText){
		String sql = listSqlText.getText();
		if (StringUtil.isNotNullNotEmpty(sql)){
			SqlBuilder sqlBuilder = new SqlBuilder();
			String[] fields = sqlBuilder.parseFields(sql);
			FieldSelectDialog dialog = new FieldSelectDialog(modelEditor,fields,targetText);
			dialog.open();			
		}
	}
	public TFMExtendAttribute getQMExtendAttribute() {
		return pfMExtendAttribute;
	}
	public Text getNodeIdFieldText() {
		return nodeIdFieldText;
	}
	public Text getNodeNameFieldText() {
		return nodeNameFieldText;
	}
	public Text getNodePIdFieldText() {
		return nodePIdFieldText;
	}
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}
	public Text getSqlMapFileText() {
		return sqlMapFileText;
	}
}
