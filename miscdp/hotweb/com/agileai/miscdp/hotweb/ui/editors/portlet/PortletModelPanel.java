package com.agileai.miscdp.hotweb.ui.editors.portlet;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.domain.portlet.PortletFuncModel;
import com.agileai.miscdp.ui.TextCheckPolicy;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.common.IniReader;
import com.agileai.domain.KeyNamePair;
import com.agileai.util.StringUtil;

/**
 * Portlet模型编辑容器
 */
public class PortletModelPanel extends Composite {
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private Combo templateCombo;
	private Text viewPageNameText;
	private Text portletClassText;
	private Text portletIdText;
	private Text funcTypeText;
	private Text funcNameText;
	private PortletModelEditor modelEditorPart; 
	private PortletFuncModel funcModel;
	private Text funcSubPkgText;
	
	private String mainPkg = null;
	private String funcSubPkg = "";
	private String portletId = "SamplePortlet";
	private Text editPageNameText;
	
	private Button generateHelpY;
	private Button generateHelpN;
	
	private Button ajaxLoadDataY;
	private Button ajaxLoadDataN;
	Text keywordText;
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public PortletModelPanel(Composite parent,PortletModelEditor editorPart, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditorPart = editorPart;
		
		funcModel = (PortletFuncModel)modelEditorPart.getFuncModel();
		ProjectConfig projectConfig = new ProjectConfig();
		String projectName = funcModel.getProjectName();
		String configFile = MiscdpUtil.getCfgFileName(projectName,DeveloperConst.PROJECT_CFG_NAME);
		projectConfig.setConfigFile(configFile);
		projectConfig.initConfig();
		this.mainPkg = projectConfig.getMainPkg();
		
		
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		toolkit.adapt(funcNameText, true, true);

		final Label label_1_1 = new Label(this, SWT.NONE);
		label_1_1.setText("功能类型");
		
		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		toolkit.adapt(funcTypeText, true, true);
		
		Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER | SWT.READ_ONLY);
		funcSubPkgText.setEditable(true);
		new TextCheckPolicy(funcSubPkgText, TextCheckPolicy.POLICY_PACKAGE_NAME);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(funcSubPkgText, true, true);
		funcSubPkgText.setForeground(ResourceUtil.getColor(255, 255, 255));
		funcSubPkgText.setBackground(ResourceUtil.getColor(128, 0, 255));
		
		final Label portletIdLabel = new Label(this, SWT.NONE);
		portletIdLabel.setText("Portlet编码");

		portletIdText = new Text(this, SWT.BORDER);
		toolkit.adapt(portletIdText, true, true);
		portletIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		portletIdText.setForeground(ResourceUtil.getColor(255, 255, 255));
		portletIdText.setBackground(ResourceUtil.getColor(128, 0, 255));
		new TextCheckPolicy(portletIdText, TextCheckPolicy.POLICY_MODEL_NAME);
		
		Label label_3 = new Label(this, SWT.NONE);
		label_3.setText("Portlet关键字");
		
		keywordText = new Text(this, SWT.BORDER);
		keywordText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(keywordText, true, true);
		
		final Label portletClassLabel = new Label(this, SWT.NONE);
		portletClassLabel.setText("Portlet处理类");

		portletClassText = new Text(this, SWT.BORDER);
		portletClassText.setEditable(false);
		toolkit.adapt(portletClassText, true, true);
		portletClassText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label viewPageNameLabel = new Label(this, SWT.NONE);
		viewPageNameLabel.setText("展现页面名称");

		viewPageNameText = new Text(this, SWT.BORDER);
		viewPageNameText.setEditable(false);
		toolkit.adapt(viewPageNameText, true, true);
		viewPageNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		Label editPageNameLabel = new Label(this, SWT.NONE);
		editPageNameLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		editPageNameLabel.setText("编辑页面名称");
		
		editPageNameText = new Text(this, SWT.BORDER);
		editPageNameText.setEditable(false);
		editPageNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(editPageNameText, true, true);
		
		Label label_2 = new Label(this, SWT.NONE);
		label_2.setText("异步加载数据");
		//new Label(this, SWT.NONE);
		Composite ajaxLoadGroup = new Composite(this, SWT.NONE);
		toolkit.adapt(ajaxLoadGroup);
		GridData gd_ajaxLoadTreeGroup = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_ajaxLoadTreeGroup.heightHint = 20;
		gd_ajaxLoadTreeGroup.widthHint = 0;
		ajaxLoadGroup.setLayoutData(gd_ajaxLoadTreeGroup);
		GridLayout gl_ajaxLoadGroup = new GridLayout(2, false);
		gl_ajaxLoadGroup.marginTop = 1;
		gl_ajaxLoadGroup.marginBottom = 1;
		gl_ajaxLoadGroup.verticalSpacing = 0;
		gl_ajaxLoadGroup.marginHeight = 0;
		gl_ajaxLoadGroup.horizontalSpacing = 14;
		ajaxLoadGroup.setLayout(gl_ajaxLoadGroup);
		ajaxLoadDataY = toolkit.createButton(ajaxLoadGroup, "是", SWT.RADIO);
		ajaxLoadDataY.setSelection(true);
		ajaxLoadDataN = new Button(ajaxLoadGroup, SWT.RADIO);
		toolkit.adapt(ajaxLoadDataN, true, true);
		ajaxLoadDataN.setText("否");
		
		
		Label generateHelpLabel = new Label(this, SWT.NONE);
		generateHelpLabel.setText("生成帮助页面");
		Composite generateHelpGroup = new Composite(this, SWT.NONE);
		toolkit.adapt(generateHelpGroup);
		GridData gd_isManageTreeGroup = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_isManageTreeGroup.heightHint = 20;
		gd_isManageTreeGroup.widthHint = 0;
		generateHelpGroup.setLayoutData(gd_isManageTreeGroup);
		GridLayout gl_isManageTreeGroup = new GridLayout(2, false);
		gl_isManageTreeGroup.marginTop = 1;
		gl_isManageTreeGroup.marginBottom = 1;
		gl_isManageTreeGroup.verticalSpacing = 0;
		gl_isManageTreeGroup.marginHeight = 0;
		gl_isManageTreeGroup.horizontalSpacing = 14;
		generateHelpGroup.setLayout(gl_isManageTreeGroup);
		generateHelpY = toolkit.createButton(generateHelpGroup, "是", SWT.RADIO);
		generateHelpN = new Button(generateHelpGroup, SWT.RADIO);
		generateHelpN.setSelection(true);
		toolkit.adapt(generateHelpN, true, true);
		generateHelpN.setText("否");
		

		final Label funcTemplateLabel = new Label(this, SWT.NONE);
		funcTemplateLabel.setText("功能模板");

		templateCombo = new Combo(this, SWT.NONE);
		toolkit.adapt(templateCombo, true, true);
		templateCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		initValues();
		registryModifier();
	}
	
	private void changeRelatePkgPath(){
		String viewJspName = funcSubPkg +"/"+portletId+"View.jsp";
		viewPageNameText.setText(viewJspName);
		
		String editJspName = funcSubPkg +"/"+portletId+"Edit.jsp";
		editPageNameText.setText(editJspName);
		
		String handlerIdClass = mainPkg+".portlets."+funcSubPkg+"."+portletId;
		portletClassText.setText(handlerIdClass);
	}

	@SuppressWarnings("rawtypes")
	private void initValues(){
		if (StringUtil.isNullOrEmpty(funcModel.getPortletName())){
			funcModel.setPortletName(portletId);
			funcModel.setFuncSubPkg(funcSubPkg);
			funcModel.setViewJspName(funcSubPkg +"/"+portletId+"View.jsp");
			funcModel.setEditJspName(funcSubPkg +"/"+portletId+"Edit.jsp");
			funcModel.setPortletClass(mainPkg+".portlets."+funcSubPkg+"."+portletId);
		}
		
		funcNameText.setText(funcModel.getFuncName());
		funcTypeText.setText(funcModel.getFuncTypeName());
		funcSubPkgText.setText(funcModel.getFuncSubPkg());
		funcSubPkg = funcModel.getFuncSubPkg();
		
		portletIdText.setText(funcModel.getPortletName());
		portletId = funcModel.getPortletName();
		
		keywordText.setText(funcModel.getKeywords());
		portletClassText.setText(funcModel.getPortletClass());
		viewPageNameText.setText(funcModel.getViewJspName());
		editPageNameText.setText(funcModel.getEditJspName());
		
		if (funcModel.isAjaxLoadData()){
			ajaxLoadDataY.setSelection(true);
			ajaxLoadDataN.setSelection(false);
		}else{
			ajaxLoadDataY.setSelection(false);
			ajaxLoadDataN.setSelection(true);
		}
		if (funcModel.isGenerateHelp()){
			generateHelpY.setSelection(true);
			generateHelpN.setSelection(false);
		}else{
			generateHelpY.setSelection(false);
			generateHelpN.setSelection(true);
		}
		
		IniReader reader = MiscdpUtil.getIniReader();
		List defValueList = reader.getList("TemplateDefine");
		for (int i=0;i < defValueList.size();i++){
			KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
			templateCombo.add(keyNamePair.getKey());
		}
		templateCombo.select(0);
	}
	
	private void registryModifier(){
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		
		funcSubPkgText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				funcSubPkg = funcSubPkgText.getText().trim();
				changeRelatePkgPath();
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();					
			}
		});

		portletIdText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				portletId = portletIdText.getText().trim();
				changeRelatePkgPath();
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();					
			}
		});
		
		keywordText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		
		portletClassText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
	
		viewPageNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		
		editPageNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		
		ajaxLoadDataY.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		ajaxLoadDataN.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		generateHelpY.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		generateHelpN.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
		templateCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditorPart.setModified(true);
				modelEditorPart.fireDirty();
			}
		});
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Text getFuncTypeText() {
		return funcTypeText;
	}

	public Text getFuncNameText() {
		return funcNameText;
	}

	public Combo getTemplateCombo() {
		return templateCombo;
	}

	public Text getViewPageNameText() {
		return viewPageNameText;
	}
	public Text getEditPageNameText() {
		return editPageNameText;
	}
	
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}
	
	public Text getPortletClassText() {
		return portletClassText;
	}

	public Text getKeywordText() {
		return keywordText;
	}
	
	public Text getPortletIdText() {
		return portletIdText;
	}
	
	public Button getGenerateHelpYButton(){
		return generateHelpY;
	}
	
	public Button getAjaxLoadDataYButton(){
		return ajaxLoadDataY;
	}
}