package com.agileai.miscdp.hotweb.ui.editors.pickfill;

import java.io.File;

import org.apache.commons.lang.BooleanUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.database.SqlBuilder;
import com.agileai.miscdp.hotweb.domain.pickfill.PickFillFuncModel;
import com.agileai.miscdp.hotweb.ui.dialogs.FieldSelectDialog;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.ui.TextCheckPolicy;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.util.StringUtil;
/**
 * 基础配置页面
 */
public class PFMBasicConfigPage extends Composite implements Modifier {
	private Text sqlMapFileText;
	private PickFillModelEditor modelEditor = null;
	private Text funcNameText;
	private Text funcTypeText;
	private Text codeFieldText;
	private Text nameFieldText;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private PFMExtendAttribute pfMExtendAttribute;
	private Text listSqlText;

	private PickFillFuncModel funcModel= null;
	private Text funcSubPkgText;
	private Combo isMultiCombo;
	
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public PFMBasicConfigPage(Composite parent,PickFillModelEditor suModelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditor = suModelEditor;
//		this.createContent();
	}
	public void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 3;
		setLayout(gridLayout);

		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		toolkit.adapt(funcNameText, true, true);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能类型");

		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		toolkit.adapt(funcTypeText, true, true);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);
		
		Label label_2 = new Label(this, SWT.NONE);
		label_2.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new TextCheckPolicy(funcSubPkgText,TextCheckPolicy.POLICY_PACKAGE_NAME);
		new Label(this, SWT.NONE);

		final Label targetPackageLabel_1_1 = new Label(this, SWT.NONE);
		targetPackageLabel_1_1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		targetPackageLabel_1_1.setText("SqlMap文件");

		sqlMapFileText = toolkit.createText(this, null);
		sqlMapFileText.setForeground(ResourceUtil.getColor(255, 255, 255));
		sqlMapFileText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		sqlMapFileText.setBackground(ResourceUtil.getColor(128, 0, 255));
		
		final Button button = new Button(this, SWT.NONE);
		toolkit.adapt(button, true, true);
		button.setText("选择");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Shell shell = new Shell();
				FileDialog fd = new FileDialog(shell,SWT.OPEN);
				fd.setText("Open");
				String projectName = funcModel.getProjectName();
				fd.setFilterPath(MiscdpUtil.getDefaultSqlMapPath(projectName));	
				
				String[] extensions = {"*.xml"};
				fd.setFilterExtensions(extensions);
				String selectedFile = fd.open();
				sqlMapFileText.setText(selectedFile);
				if (selectedFile != null){
					String sqlMapFilePath = selectedFile.substring(0,selectedFile.lastIndexOf(File.separator)+1)+"SomeListSelect.xml";
					sqlMapFileText.setText(sqlMapFilePath);
				}
			}
		});
		
		final Label codeFieldlabel = new Label(this, SWT.NONE);
		codeFieldlabel.setText("编码字段");

		codeFieldText = new Text(this, SWT.BORDER);
		codeFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(codeFieldText);
			}
		});
		toolkit.adapt(codeFieldText, true, true);
		codeFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE).setText("*");;		
		
		
		final Label nameFieldlabel = new Label(this, SWT.NONE);
		nameFieldlabel.setText("名称字段");

		nameFieldText = new Text(this, SWT.BORDER);
		nameFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(nameFieldText);
			}
		});
		toolkit.adapt(nameFieldText, true, true);
		nameFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE).setText("*");;		
		
		
		final Label targetPackageLabel_1_1_1 = new Label(this, SWT.NONE);
		targetPackageLabel_1_1_1.setText("是否多选");

		isMultiCombo = new Combo(this, SWT.NONE);
		isMultiCombo.add("Yes");
		isMultiCombo.add("No");
		toolkit.adapt(isMultiCombo, true, true);
		isMultiCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);
		
		
		final Composite formComposite = new Composite(this, SWT.NONE);
		formComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		formComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.numColumns = 3;
		gridLayout_4.marginHeight = 2;
		gridLayout_4.horizontalSpacing = 0;
		gridLayout_4.verticalSpacing = 0;
		gridLayout_4.marginWidth = 0;
		formComposite.setLayout(gridLayout_4);

		final TabFolder tabFolder = new TabFolder(formComposite, SWT.NONE);
		final GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 3);
		gd_tabFolder.widthHint = 494;
		gd_tabFolder.heightHint = 254;
		tabFolder.setLayoutData(gd_tabFolder);
		toolkit.adapt(tabFolder, true, true);
		
		final TabItem tabItem_1 = new TabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("列表SQL");

		listSqlText = new Text(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		toolkit.adapt(listSqlText, true, true);
		tabItem_1.setControl(listSqlText);

		final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("扩展属性");
		pfMExtendAttribute = new PFMExtendAttribute(tabFolder,SWT.NONE);
		tabItem.setControl(pfMExtendAttribute);
	}
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}
	public void setFuncModel(PickFillFuncModel standardFuncModel) {
		this.funcModel = standardFuncModel;
	}
	public PickFillFuncModel buildConfig(){
		funcModel.setFuncName(funcNameText.getText());
//		funcModel.setFuncType(funcTypeText.getText());
		funcModel.setFuncSubPkg(funcSubPkgText.getText());
		funcModel.setCodeField(codeFieldText.getText());
		funcModel.setNameField(nameFieldText.getText());
		
		String selectedValue = isMultiCombo.getItem(isMultiCombo.getSelectionIndex());
		funcModel.setIsMultiSelect(BooleanUtils.toBoolean(selectedValue));
		
		
		funcModel.setListSql(listSqlText.getText());
		funcModel.setSqlMapFile(sqlMapFileText.getText());
		funcModel.setServiceId(pfMExtendAttribute.getServiceIdText().getText());
		funcModel.setImplClassName(pfMExtendAttribute.getImplClassNameText().getText());
		funcModel.setInterfaceName(pfMExtendAttribute.getInterfaceNameText().getText());
		funcModel.setListJspName(pfMExtendAttribute.getListJspNameText().getText());

		funcModel.setExportCsv(pfMExtendAttribute.getExportCsvBtn().getSelection());
		funcModel.setExportXls(pfMExtendAttribute.getExportXslBtn().getSelection());
		funcModel.setPagination(pfMExtendAttribute.getPaginationBtn().getSelection());
		funcModel.setSortAble(pfMExtendAttribute.getSortListBtn().getSelection());
		funcModel.setTemplate(pfMExtendAttribute.getTemplateCombo().getItem(0));
		return this.funcModel;
	}

	public void initValues() {
		funcNameText.setText(this.funcModel.getFuncName());
		funcTypeText.setText(this.funcModel.getFuncTypeName());
		funcSubPkgText.setText(this.funcModel.getFuncSubPkg());
		listSqlText.setText(this.funcModel.getListSql().trim());
		codeFieldText.setText(this.funcModel.getCodeField());
		nameFieldText.setText(this.funcModel.getNameField());
		if (this.funcModel.getIsMultiSelect()){
			isMultiCombo.select(0);	
		}else{
			isMultiCombo.select(1);
		}
		sqlMapFileText.setText(this.funcModel.getSqlMapFile());
		
		pfMExtendAttribute.getServiceIdText().setText(funcModel.getServiceId());
		pfMExtendAttribute.getImplClassNameText().setText(funcModel.getImplClassName());
		pfMExtendAttribute.getInterfaceNameText().setText(funcModel.getInterfaceName());
		pfMExtendAttribute.getListJspNameText().setText(funcModel.getListJspName());
		pfMExtendAttribute.getTemplateCombo().select(0);
		
		pfMExtendAttribute.getExportCsvBtn().setSelection(funcModel.isExportCsv());
		pfMExtendAttribute.getExportXslBtn().setSelection(funcModel.isExportXls());
		pfMExtendAttribute.getPaginationBtn().setSelection(funcModel.isPagination());
		pfMExtendAttribute.getSortListBtn().setSelection(funcModel.isSortAble());
	}
	public Text getListSqlText() {
		return listSqlText;
	}
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}
	
	public void registryModifier() {
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		funcSubPkgText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				if (!funcSubPkgText.getText().equals(funcModel.getFuncSubPkg())){
					modelEditor.setModified(true);
					modelEditor.fireDirty();				
				}
			}
		});
		codeFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		nameFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		isMultiCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});		
		pfMExtendAttribute.getPaginationBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getSortListBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getExportCsvBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getExportXslBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getTemplateCombo().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getListJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getInterfaceNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getImplClassNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		pfMExtendAttribute.getServiceIdText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		listSqlText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sqlMapFileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
	private void openFieldSelectDialog(Text targetText){
		String sql = listSqlText.getText();
		if (StringUtil.isNotNullNotEmpty(sql)){
			SqlBuilder sqlBuilder = new SqlBuilder();
			String[] fields = sqlBuilder.parseFields(sql);
			FieldSelectDialog dialog = new FieldSelectDialog(modelEditor,fields,targetText);
			dialog.open();			
		}
	}
	
	public PFMExtendAttribute getQMExtendAttribute() {
		return pfMExtendAttribute;
	}
	public Text getCodeFieldText() {
		return codeFieldText;
	}
	public Text getNameFieldText() {
		return nameFieldText;
	}
	public Text getSqlMapFileText(){
		return this.sqlMapFileText;
	}
}
