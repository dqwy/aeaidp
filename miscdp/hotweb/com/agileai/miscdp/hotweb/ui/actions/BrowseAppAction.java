package com.agileai.miscdp.hotweb.ui.actions;

import java.io.IOException;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import com.agileai.miscdp.hotweb.ConsoleHandler;

public class BrowseAppAction implements IObjectActionDelegate{
    private IWorkbenchPart targetPart;  
    
	public void run(IAction action) {
		ISelection selection = targetPart.getSite().getSelectionProvider().getSelection();
		IStructuredSelection curSelection = (IStructuredSelection)selection;
		IJavaProject javaProject = (IJavaProject)curSelection.getFirstElement();
		String path = javaProject.getProject().getLocation().toFile().getAbsolutePath();
		try {
			Runtime.getRuntime().exec("explorer " + path);
		} catch (IOException e) {
			ConsoleHandler.error(e.getLocalizedMessage());
		}
	}

	public void setActivePart(IAction action, IWorkbenchPart workbenchPart) {
        this.targetPart = workbenchPart;  
	}
	public void selectionChanged(IAction action, ISelection selection) {
	}	
}