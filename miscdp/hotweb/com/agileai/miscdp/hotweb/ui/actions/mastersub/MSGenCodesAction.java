package com.agileai.miscdp.hotweb.ui.actions.mastersub;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

import com.agileai.miscdp.hotweb.domain.mastersub.MasterSubFuncModel;
import com.agileai.miscdp.hotweb.domain.mastersub.SubTableConfig;
import com.agileai.miscdp.hotweb.generator.mastersub.MSHandlerCfgGenerator;
import com.agileai.miscdp.hotweb.generator.mastersub.MSHandlerGenerator;
import com.agileai.miscdp.hotweb.generator.mastersub.MSJspPagGenerator;
import com.agileai.miscdp.hotweb.generator.mastersub.MSServiceCfgGenerator;
import com.agileai.miscdp.hotweb.generator.mastersub.MSServiceClassGenerator;
import com.agileai.miscdp.hotweb.generator.mastersub.MSServiceInterfaceGenerator;
import com.agileai.miscdp.hotweb.generator.mastersub.MSSqlMapCfgGenerator;
import com.agileai.miscdp.hotweb.generator.mastersub.MSSqlMapGenerator;
import com.agileai.miscdp.hotweb.ui.actions.BaseGenCodesAction;
import com.agileai.miscdp.hotweb.ui.dialogs.CodeGenConfigDialog;
import com.agileai.miscdp.hotweb.ui.editors.mastersub.MasterSubModelEditor;
import com.agileai.miscdp.hotweb.ui.editors.mastersub.MasterSubModelEditorContributor;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.domain.KeyNamePair;
import com.agileai.util.FileUtil;
/**
 * 代码生成动作
 */
public class MSGenCodesAction extends BaseGenCodesAction{
	private static final String GenOptionSection = "MasterSubModelEditor.GeneratorOption";
	
	public MSGenCodesAction() {
	}
	public void setContributor(MasterSubModelEditorContributor contributor) {
		this.contributor = contributor;
	}
	public void run() {
		MasterSubModelEditor modelEditor = (MasterSubModelEditor)this.getModelEditor();
		Shell shell = modelEditor.getSite().getShell();
    	if (modelEditor.isDirty()){
    		String confirmTitle = MiscdpUtil.getIniReader().getValue("DialogInfomation","ConfirmTitle");
    		String confirmMsg = MiscdpUtil.getIniReader().getValue("DialogInfomation","IsSaveFirst");
    		boolean saveFirst = MessageDialog.openConfirm(shell, confirmTitle, confirmMsg);
    		if (saveFirst){
    			modelEditor.doSave(new NullProgressMonitor());
    		}else{
    			return;
    		}
    	}		
    	funcModel = modelEditor.buildFuncModel();
    	CodeGenConfigDialog configDialog = new CodeGenConfigDialog(shell);
		List<KeyNamePair> inputGenList = new ArrayList<KeyNamePair>();
		inputGenList = MiscdpUtil.getIniReader().getList(GenOptionSection);
		configDialog.setInputGenList(inputGenList);
		configDialog.open();
		if (configDialog.getReturnCode() != Dialog.OK){
			return;
		}
		genertorMap = configDialog.getSelectedKeyNamePairs();
		
		IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
		try {
	    	String projectName = funcModel.getProjectName();
	    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			progressService.runInUI(progressService,this,project);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void run(IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {
		monitor.beginTask("init model values:", 1000);
		monitor.subTask("set model values....");
		monitor.worked(50);

    	String projectName = funcModel.getProjectName();
    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
    	String tableName = ((MasterSubFuncModel)funcModel).getTableName();
    	File xmlFile = getXmlFile();
//    	this.funcModel = funcModel.buildFuncModel(xmlFile);
    	
		if (genertorMap.containsKey(GEN_OPTION_JSPPAGE)){
			monitor.subTask("generating jsp files....");
			monitor.worked(200);
			MSJspPagGenerator sMJspPagGenerator = new MSJspPagGenerator();
			sMJspPagGenerator.setXmlFile(xmlFile);
			sMJspPagGenerator.setTemplate(funcModel.getTemplate());
			sMJspPagGenerator.setFuncModel((MasterSubFuncModel)funcModel);

	    	String listJspName = project.getLocation().toString()+"/WebRoot/jsp/"+((MasterSubFuncModel)funcModel).getListJspName();
	    	String detailJspName = project.getLocation().toString()+"/WebRoot/jsp/"+((MasterSubFuncModel)funcModel).getDetailJspName();
			sMJspPagGenerator.setListJspFile(listJspName);
			sMJspPagGenerator.setDetailJspFile(detailJspName);
			List<SubTableConfig> subTableConfigList = ((MasterSubFuncModel)funcModel).getSubTableConfigs();
			for (int i=0;i < subTableConfigList.size();i++){
				SubTableConfig subTableConfig = subTableConfigList.get(i);
				String editMode = subTableConfig.getEditMode();
				if (SubTableConfig.LIST_DETAIL_MODE.equals(editMode)){
					String boxJspFile = subTableConfig.getPboxJspName();
					boxJspFile = project.getLocation().toString()+"/WebRoot/jsp/"+boxJspFile;
					sMJspPagGenerator.getBoxJspFileList().add(boxJspFile);
					sMJspPagGenerator.getBoxSubTabIdList().add(subTableConfig.getSubTableId());
				}
			}
			sMJspPagGenerator.generate();
		}		
		if (genertorMap.containsKey(GEN_OPTION_SQLMAP_DEFINE)){
			monitor.subTask("generating SqlMap files....");
			monitor.worked(300);
			MSSqlMapGenerator sMSqlMapGenerator = new MSSqlMapGenerator(projectName);
			String[] colNames = MiscdpUtil.getColNames(((MasterSubFuncModel)funcModel).getPageFormFields());
			String findRecordsSql = funcModel.getListSql();
			
			MasterSubFuncModel masterSubFuncModel = (MasterSubFuncModel)funcModel;
			sMSqlMapGenerator.setSubTableConfigList(masterSubFuncModel.getSubTableConfigs());
			sMSqlMapGenerator.setColNames(colNames);
			sMSqlMapGenerator.setFindRecordsSql(findRecordsSql);
			sMSqlMapGenerator.setTableName(tableName);
	    	String abstSqlMapPath = project.getLocation().toString()+"/src/sqlmap";
	    	File tempFile = new File(abstSqlMapPath);
	    	if (!tempFile.exists()){
	    		tempFile.mkdirs();
	    	}
	    	String sqlMapFileName = MiscdpUtil.getValidName(tableName); 
	    	String sqlMapFile = abstSqlMapPath + "/" + sqlMapFileName+".xml";
	    	sMSqlMapGenerator.setSqlMapFile(sqlMapFile);
	    	sMSqlMapGenerator.setFuncModel(masterSubFuncModel);
			sMSqlMapGenerator.generate();
		}
		if (genertorMap.containsKey(GEN_OPTION_SQLMAP_INDEX)){
			monitor.subTask("generating SqlMap config files....");
			monitor.worked(400);
			MSSqlMapCfgGenerator sMSqlMapCfgGenerator = new MSSqlMapCfgGenerator();
			String abstSqlMapCfgPath = project.getLocation().toString()+"/src/SqlMapConfig.xml";
			sMSqlMapCfgGenerator.setTableName(tableName);
			sMSqlMapCfgGenerator.setConfigFile(abstSqlMapCfgPath);
			sMSqlMapCfgGenerator.generate();
		}
		
		if (genertorMap.containsKey(GEN_OPTION_SERVICE_CONFIG)){
			monitor.subTask("generating Service config files....");
			monitor.worked(500);
			MSServiceCfgGenerator sMServiceCfgGenerator = new MSServiceCfgGenerator();
			String abstServiceCfgPath = project.getLocation().toString()+"/src/ServiceContext.xml";
			sMServiceCfgGenerator.setConfigFile(abstServiceCfgPath);
			sMServiceCfgGenerator.setFuncModel((MasterSubFuncModel)funcModel);
			sMServiceCfgGenerator.generate();	
		}
		
		if (genertorMap.containsKey(GEN_OPTION_HANDLER_CONFIG)){
			monitor.subTask("generating Handler config files....");
			monitor.worked(600);
			MSHandlerCfgGenerator sMHandlerCfgGenerator = new MSHandlerCfgGenerator();
			String abstHandlerCfgPath = project.getLocation().toString()+"/src/HandlerContext.xml";
			sMHandlerCfgGenerator.setConfigFile(abstHandlerCfgPath);
			sMHandlerCfgGenerator.setFuncModel((MasterSubFuncModel)funcModel);
			sMHandlerCfgGenerator.generate();	
		}
		
		String srcDir = project.getLocation().toString()+"/src";
		if (genertorMap.containsKey(GEN_OPTION_HANDLER_CLASS)){
			monitor.subTask("generating Handler java files....");
			monitor.worked(700);
			MSHandlerGenerator sMHandlerGenerator = new MSHandlerGenerator();
			sMHandlerGenerator.setSrcPath(srcDir);
			MasterSubFuncModel msFuncModel = (MasterSubFuncModel)funcModel;
			String listHandlerClassName = msFuncModel.getListHandlerClass();
			sMHandlerGenerator.setListHandlerClassName(listHandlerClassName);
			String editHandlerClassName = msFuncModel.getEditHandlerClass();
			sMHandlerGenerator.setEditHandlerClassName(editHandlerClassName);
			
			List<SubTableConfig> subTableConfigList = ((MasterSubFuncModel)funcModel).getSubTableConfigs();
			for (int i=0;i < subTableConfigList.size();i++){
				SubTableConfig subTableConfig = subTableConfigList.get(i);
				String editMode = subTableConfig.getEditMode();
				if (SubTableConfig.LIST_DETAIL_MODE.equals(editMode)){
					String handlerClass = subTableConfig.getPboxHandlerClass();
					sMHandlerGenerator.getPboxHandlerClassNameList().add(handlerClass);
				}
			}
			sMHandlerGenerator.setXmlFile(xmlFile);
			sMHandlerGenerator.generate();			
		}
		
		if (genertorMap.containsKey(GEN_OPTION_SERVICE_IMPL)){
			monitor.subTask("generating Service Impl java files....");
			monitor.worked(800);
			
			MSServiceClassGenerator sMServiceClassGenerator = new MSServiceClassGenerator();
			String implClassName = funcModel.getImplClassName();
			File implJavaFile = FileUtil.createJavaFile(srcDir, implClassName);
			String implJavaFilePath = implJavaFile.getAbsolutePath();
			sMServiceClassGenerator.setJavaFile(implJavaFilePath);
			sMServiceClassGenerator.setXmlFile(xmlFile);
			sMServiceClassGenerator.generate();
			
			try {
				MiscdpUtil.getJavaFormater().format(implJavaFilePath, sMServiceClassGenerator.getCharencoding());	
			} catch (Exception e) {
				e.printStackTrace();
				throw new InvocationTargetException(e);
			}
		}

		if (genertorMap.containsKey(GEN_OPTION_SERVICE_INTERFACE)){
			monitor.subTask("generating Service Interface java files....");
			monitor.worked(900);
			
			MSServiceInterfaceGenerator sMServiceInterfaceGenerator = new MSServiceInterfaceGenerator();
			String interfaceClassName = funcModel.getInterfaceName();
			File interfaceJavaFile = FileUtil.createJavaFile(srcDir, interfaceClassName);
			
			String interfaceJavaFilePath = interfaceJavaFile.getAbsolutePath();
			sMServiceInterfaceGenerator.setJavaFile(interfaceJavaFilePath);
			sMServiceInterfaceGenerator.setXmlFile(xmlFile);
			sMServiceInterfaceGenerator.generate();
			try {
				MiscdpUtil.getJavaFormater().format(interfaceJavaFilePath, sMServiceInterfaceGenerator.getCharencoding());
			} catch (IOException e1) {
				e1.printStackTrace();
				throw new InvocationTargetException(e1);
			}
		}
		
		try {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}
