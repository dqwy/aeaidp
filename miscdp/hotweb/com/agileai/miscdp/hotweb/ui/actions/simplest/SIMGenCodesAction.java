﻿package com.agileai.miscdp.hotweb.ui.actions.simplest;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

import com.agileai.miscdp.hotweb.domain.simplest.SimplestFuncModel;
import com.agileai.miscdp.hotweb.generator.simplest.SIMHandlerCfgGenerator;
import com.agileai.miscdp.hotweb.generator.simplest.SIMHandlerGenerator;
import com.agileai.miscdp.hotweb.generator.simplest.SIMJspPagGenerator;
import com.agileai.miscdp.hotweb.ui.actions.BaseGenCodesAction;
import com.agileai.miscdp.hotweb.ui.editors.simplest.SimplestModelEditor;
import com.agileai.miscdp.hotweb.ui.editors.simplest.SimplestModelEditorContributor;
import com.agileai.miscdp.hotweb.ui.editors.simplest.SimplestModelPanel;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.FileUtil;
import com.agileai.util.StringUtil;
/**
 * 最简功能模型生成代码动作 
 */
public class SIMGenCodesAction extends BaseGenCodesAction{
	public SIMGenCodesAction() {
	}
	public void setContributor(SimplestModelEditorContributor contributor) {
		this.contributor = contributor;
	}
	public void run() {
		SimplestModelEditor modelEditor = (SimplestModelEditor)this.getModelEditor();
		Shell shell = modelEditor.getSite().getShell();
		
		SimplestModelPanel simplestModelPanel = modelEditor.getSimplestModelPanel();
		String funcSubPkg = simplestModelPanel.getFuncSubPkgText().getText();
		
		if (StringUtil.isNullOrEmpty(funcSubPkg)){
			DialogUtil.showMessage(shell,"消息提示","目录编码不能为空!",DialogUtil.MESSAGE_TYPE.WARN);
			return;
		}
		
    	if (modelEditor.isDirty()){
    		String confirmTitle = MiscdpUtil.getIniReader().getValue("DialogInfomation","ConfirmTitle");
    		String confirmMsg = MiscdpUtil.getIniReader().getValue("DialogInfomation","IsSaveFirst");
    		boolean saveFirst = MessageDialog.openConfirm(shell, confirmTitle, confirmMsg);
    		if (saveFirst){
    			modelEditor.doSave(new NullProgressMonitor());
    		}else{
    			return;
    		}
    	}
		funcModel = modelEditor.buildFuncModel();
		
		IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
		try {
			String projectName = funcModel.getProjectName();
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			progressService.runInUI(progressService,this,project);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void run(IProgressMonitor monitor) throws InvocationTargetException,
			InterruptedException {
		monitor.beginTask("init model values:", 500);
		monitor.subTask("set model values....");
		monitor.worked(50);
		
		monitor.subTask("generating jsp files....");
		monitor.worked(200);
		SIMJspPagGenerator sMJspPagGenerator = new SIMJspPagGenerator();
		File xmlFile = getXmlFile();
		sMJspPagGenerator.setXmlFile(xmlFile);
		
    	String projectName = funcModel.getProjectName();
//		this.funcModel = funcModel.buildFuncModel(xmlFile);
		
    	sMJspPagGenerator.setTemplate(funcModel.getTemplate());
    	
    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
    	String defaultJspName = project.getLocation().toString()+"/WebRoot/jsp/"+((SimplestFuncModel)funcModel).getDefaultJspName();
		sMJspPagGenerator.setDefaultJspFile(defaultJspName);
		sMJspPagGenerator.generate();
		
		monitor.subTask("generating Handler config files....");
		monitor.worked(300);
		SIMHandlerCfgGenerator sMHandlerCfgGenerator = new SIMHandlerCfgGenerator();
		String abstHandlerCfgPath = project.getLocation().toString()+"/src/HandlerContext.xml";
		sMHandlerCfgGenerator.setConfigFile(abstHandlerCfgPath);
		sMHandlerCfgGenerator.setFuncModel((SimplestFuncModel)funcModel);
		sMHandlerCfgGenerator.generate();
		
		monitor.subTask("generating Handler java files....");
		monitor.worked(400);
		
		String srcDir = project.getLocation().toString()+"/src";
		SIMHandlerGenerator sMHandlerGenerator = new SIMHandlerGenerator();
		SimplestFuncModel simplestFuncModel = (SimplestFuncModel)funcModel;
		String handlerClassName = simplestFuncModel.getHandlerClass();
		File javaFile = FileUtil.createJavaFile(srcDir, handlerClassName);
		sMHandlerGenerator.setJavaFile(javaFile);
		sMHandlerGenerator.setXmlFile(xmlFile);
		sMHandlerGenerator.generate();
		
		try {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}
