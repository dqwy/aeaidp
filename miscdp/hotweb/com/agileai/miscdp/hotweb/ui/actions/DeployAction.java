package com.agileai.miscdp.hotweb.ui.actions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.progress.IWorkbenchSiteProgressService;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.ConsoleHandler;
import com.agileai.miscdp.hotweb.domain.DeployResource;
import com.agileai.miscdp.hotweb.domain.DeployResource.DeployTypes;
import com.agileai.miscdp.hotweb.domain.DeployResource.FileTypes;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.ui.dialogs.DeployDialog;
import com.agileai.miscdp.server.DeployableZip;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.FileUtil;
import com.agileai.miscdp.util.JavaModelUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ZipHelper;

public class DeployAction implements IViewActionDelegate{
    private IWorkbenchPart targetPart;
    
    private List<String> moduleNames = null;
    private HashMap<String,String> moduleJspPathMap = null;
    private HashMap<String,String> moduleJavaPathMap = null;
    
    private List<DeployResource> selectedResourceList = null; 
    private List<String> selectedModuleNames = null;

    private List<String> deleteDirs = null;
	private List<String> folderPathList = null;
	private List<String> packagePathList = null;
    
	private String tempDir = null;
	private File tempDirFile = null;
	private String tempDeployDir = null;
    
	public void run(IAction action) {
	    this.moduleNames = new ArrayList<String>();
	    this.moduleJspPathMap = new HashMap<String,String>();
	    this.moduleJavaPathMap = new HashMap<String,String>();
	    this.selectedResourceList = new ArrayList<DeployResource>();
	    
	    this.selectedModuleNames = new ArrayList<String>();
	    
	    this.deleteDirs = new ArrayList<String>();
	    this.folderPathList = new ArrayList<String>();
	    this.packagePathList = new ArrayList<String>();
	    
	    this.tempDir = null;
	    this.tempDirFile = null;
	    this.tempDeployDir = null;
		
		ISelection selection = targetPart.getSite().getSelectionProvider().getSelection();
		IStructuredSelection curSelection = (IStructuredSelection)selection;
		List<?> resources = curSelection.toList();

		if (resources != null && resources.size() > 0){
			if (resources.size() == 1 && resources.get(0) instanceof IJavaProject){
				IJavaProject javaProject = retriveJavaProject(resources.get(0));
				this.parseModuleNames(javaProject);
				deployApplication(javaProject);
			}
			else{
				IJavaProject javaProject = retriveJavaProject(resources.get(0));
				if (javaProject == null){
					DialogUtil.showInfoMessage("select resource is not valid !");
					return;
				}
				
				this.parseModuleNames(javaProject);
				
				if (isAllModuleResources(resources)){
					deployModules(resources, javaProject);
				}else{
					deployAppFiles(resources, javaProject);
				}
			}
		}
	}

	private boolean isAllModuleResources(List<?> resources){
		boolean result = true;
		for (int i=0;i < resources.size();i++){
			Object object = resources.get(i);
			if (object instanceof IResource){
				IResource resource = (IResource)object;
				String resPath = resource.getFullPath().makeAbsolute().toString();
				if (!isValidModuleJspPath(resPath) && !isValidModuleJavaPath(resPath)){
					result = false;
					break;
				}
			}
			else if (object instanceof IJavaElement){
				IJavaElement javaElement = (IJavaElement)object;
				String resPath = javaElement.getPath().makeAbsolute().toString();
				if (!isValidModuleJavaPath(resPath)){
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	private boolean isValidModuleJspPath(String resPath){
		boolean result = false;
		Iterator<String> keys = moduleJspPathMap.keySet().iterator();
		while (keys.hasNext()){
			String moduleJspPath = keys.next();
			if (resPath.startsWith(moduleJspPath)){
				result = true;
				String moduleName = moduleJspPathMap.get(moduleJspPath);
				if (!selectedModuleNames.contains(moduleName)){
					this.selectedModuleNames.add(moduleName);					
				}
				break;
			}			
		}
		return result;
	}
	
	private boolean isValidModuleJavaPath(String resPath){
		boolean result = false;
		Iterator<String> keys = moduleJavaPathMap.keySet().iterator();
		while (keys.hasNext()){
			String moduleJavaPath = keys.next();
			if (resPath.startsWith(moduleJavaPath)){
				result = true;
				String moduleName = moduleJavaPathMap.get(moduleJavaPath);
				if (!selectedModuleNames.contains(moduleName)){
					this.selectedModuleNames.add(moduleName);					
				}
				break;
			}
		}
		return result;
	}
	
	private void initTempDirs(IJavaProject javaProject,String appName){
		IProject project = javaProject.getProject();
		File projectFile = project.getLocation().toFile().getAbsoluteFile();
		String parentPath = projectFile.getParentFile().getPath();
		this.tempDeployDir = parentPath + "/tempDeploy";
		File tempDeployFile = new File(tempDeployDir);
		if (!tempDeployFile.exists()){
			tempDeployFile.mkdir();
		}
		
		this.tempDir = parentPath + "/tempDir/"+appName;
		this.tempDirFile = new File(tempDir);
		if (tempDirFile.exists()){
			if (tempDirFile.listFiles().length > 0){
				try {
					FileUtil.deleteWholeDirectory(tempDirFile);
					tempDirFile.mkdirs();					
				} catch (Exception e) {
					MiscdpPlugin.getDefault().logError(e.getLocalizedMessage(),e);
				}
			}
		}else{
			tempDirFile.mkdirs();
		}
	}
	
	private DeployableZip buildApplicaitonZip(IJavaProject javaProject,String appName,boolean needReload) throws IOException{
		DeployableZip deployableZip = new DeployableZip();
		deployableZip.setType(DeployResource.FileTypes.Applicaiton);
		deployableZip.setNeedReload(needReload);
		
		ConsoleHandler.info("prepare zip resource ...");
		
		this.initTempDirs(javaProject, appName);
		
		File sourceWeb = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot"));
		List<String> excludeFiles = new ArrayList<String>();
		excludeFiles.add("module");
		excludeFiles.add(".svn");
		excludeFiles.add(".CVS");
		FileUtil.copyWebRoot(sourceWeb,tempDirFile,true,excludeFiles);
		
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
		String subDir = projectConfig.getMainPkg().replaceAll("\\.", "/");
		
		File sqlmapDesDir = new File(tempDir+"/WEB-INF/classes/sqlmap");
		
		for (int i=0;i < moduleNames.size();i++){
			String moduleName = moduleNames.get(i);
			File tempClassesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName);
			tempClassesDir.mkdirs();
			
			File sourceModuleClassesFile = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot/WEB-INF/classes/"+subDir+"/module/" + moduleName));
			if (sourceModuleClassesFile.exists() && sourceModuleClassesFile.listFiles().length > 0){
				FileUtil.copyDir(sourceModuleClassesFile,tempClassesDir,true,new ArrayList<String>());	
			}
			
			File tempHandlerClassesDesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/"+subDir+"/module/"+moduleName+"/handler");
			tempHandlerClassesDesDir.mkdirs();
			File tempHandleClassSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/handler");
			FileUtil.copyDir(tempHandleClassSrcDir,tempHandlerClassesDesDir,true,new ArrayList<String>());	
			
			File tempServiceClassesDesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/"+subDir+"/module/"+moduleName+"/service");
			tempServiceClassesDesDir.mkdirs();
			File tempServiceClassSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/service");
			FileUtil.copyDir(tempServiceClassSrcDir,tempServiceClassesDesDir,true,new ArrayList<String>());	
			
			File tempSqlmapSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/sqlmap");
			FileUtil.copyDir(tempSqlmapSrcDir,sqlmapDesDir,true,new ArrayList<String>());	
			
			FileUtil.deleteWholeDirectory(tempHandleClassSrcDir);
			FileUtil.deleteWholeDirectory(tempServiceClassSrcDir);
			FileUtil.deleteWholeDirectory(tempSqlmapSrcDir);
		}
		
		ConsoleHandler.info("do zip files ...");
		
		ZipHelper zipUtil = new ZipHelper();
		String tempZipFile = tempDeployDir + "/" + appName + ".zip";
		zipUtil.doZip(tempDirFile.getAbsolutePath(),tempZipFile);
		
		DataSource source = new FileDataSource(new File(tempZipFile));
		deployableZip.setDataHandler(new DataHandler(source));
		return deployableZip;
	}
	
	private DeployableZip buildModulesZip(IJavaProject javaProject,String appName,List<String> selectedModuleNames,boolean needReload) throws IOException{
		DeployableZip deployableZip = new DeployableZip();
		deployableZip.setType(DeployResource.FileTypes.Module);
		deployableZip.setNeedReload(needReload);
		
		ConsoleHandler.info("prepare zip resource ...");
		
		this.initTempDirs(javaProject, appName);
		
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
		String subDir = projectConfig.getMainPkg().replaceAll("\\.", "/");
		
		File sqlmapDesDir = new File(tempDir+"/WEB-INF/classes/sqlmap");
		sqlmapDesDir.mkdirs();
		
		for (int i=0;i < selectedModuleNames.size();i++){
			String moduleName = selectedModuleNames.get(i);
			File tempClassesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName);
			tempClassesDir.mkdirs();
			
			File sourceModuleClassesFile = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot/WEB-INF/classes/"+subDir+"/module/" + moduleName));
			if (sourceModuleClassesFile.exists() && sourceModuleClassesFile.listFiles().length > 0){
				FileUtil.copyDir(sourceModuleClassesFile,tempClassesDir,true,new ArrayList<String>());	
			}
			
			File sourceModuleJspFile = MiscdpUtil.getProjectFile(appName, new Path("/WebRoot/jsp/" + moduleName));
			if (sourceModuleJspFile.exists() && sourceModuleJspFile.listFiles().length > 0){
				File tempJspDesDir = new File(tempDir+"/jsp/" + moduleName);
				tempJspDesDir.mkdirs();
				
				FileUtil.copyDir(sourceModuleJspFile,tempJspDesDir,true,new ArrayList<String>());	
			}
			
			File tempHandlerClassesDesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/"+subDir+"/module/"+moduleName+"/handler");
			tempHandlerClassesDesDir.mkdirs();
			File tempHandleClassSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/handler");
			FileUtil.copyDir(tempHandleClassSrcDir,tempHandlerClassesDesDir,true,new ArrayList<String>());	
			
			File tempServiceClassesDesDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/"+subDir+"/module/"+moduleName+"/service");
			tempServiceClassesDesDir.mkdirs();
			File tempServiceClassSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/service");
			FileUtil.copyDir(tempServiceClassSrcDir,tempServiceClassesDesDir,true,new ArrayList<String>());	
			
			File tempSqlmapSrcDir = new File(tempDir+"/WEB-INF/modules/" + moduleName+"/sqlmap");
			FileUtil.copyDir(tempSqlmapSrcDir,sqlmapDesDir,true,new ArrayList<String>());	
			
			FileUtil.deleteWholeDirectory(tempHandleClassSrcDir);
			FileUtil.deleteWholeDirectory(tempServiceClassSrcDir);
			FileUtil.deleteWholeDirectory(tempSqlmapSrcDir);
		}
		
		ConsoleHandler.info("do zip files ...");
		
		ZipHelper zipUtil = new ZipHelper();
		String tempZipFile = tempDeployDir + "/" + appName + ".zip";
		zipUtil.doZip(tempDirFile.getAbsolutePath(),tempZipFile);
		
		DataSource source = new FileDataSource(new File(tempZipFile));
		deployableZip.setDataHandler(new DataHandler(source));
		
		return deployableZip;
	}
	
	private void checkParentFolder(String compilePath){
		for (int i=0;i < this.folderPathList.size();i++){
			String folderPath = this.folderPathList.get(i);
			if (!compilePath.equals(folderPath) && compilePath.startsWith(folderPath)){
				this.folderPathList.remove(i);
				break;
			}
		}
		for (int i=0;i < this.packagePathList.size();i++){
			String packagePath = this.packagePathList.get(i);
			if (!compilePath.equals(packagePath) && compilePath.startsWith(packagePath)){
				this.folderPathList.remove(i);
				break;
			}		
		}
	}
	
	private String parseXmlFileName(String path){
		String result = null;
		int lastSplashIndex = path.lastIndexOf("/");
		result = path.substring(lastSplashIndex+1,path.length());
		return result;
	}
	
	private DeployableZip buildAppFilesZip(IJavaProject javaProject,String appName,List<DeployResource> selectedDeployFilesList
			,boolean needReload) throws IOException{
		DeployableZip deployableZip = new DeployableZip();
		deployableZip.setType(DeployResource.FileTypes.Files);
		deployableZip.setNeedReload(needReload);
		
		this.initTempDirs(javaProject, appName);
		
		ConsoleHandler.info("prepare zip resource ...");
		
		File projectFile = javaProject.getProject().getLocation().toFile().getAbsoluteFile();
		
		for (int i=0;i < selectedDeployFilesList.size();i++){
			DeployResource deployResource = selectedDeployFilesList.get(i);
			String compilePath = deployResource.getCompilePath();
			checkParentFolder(compilePath);
			
			String currentPath = projectFile.getAbsolutePath()+ File.separator + "WebRoot" + compilePath;
			if (FileTypes.Folder.equals(deployResource.getFileType())
					|| FileTypes.Package.equals(deployResource.getFileType())){
				File tempResouceDesDir = null;
				if (deployResource.isSource() && compilePath.indexOf("/module/") > -1){
					String moduleName = parseModuleName(compilePath);
					if (compilePath.endsWith("sqlmap")){
						compilePath = "/WEB-INF/classes/sqlmap";
					}else{
						compilePath = compilePath.replaceAll("/classes/","/modules/"+moduleName+"/");
					}
					tempResouceDesDir = new File(tempDir + compilePath);
				}else{
					tempResouceDesDir = new File(tempDir + compilePath);
				}
				if (!tempResouceDesDir.exists()){
					tempResouceDesDir.mkdirs();
				}
				File tempResouceSrcDir = new File(currentPath);
				FileUtil.copyDir(tempResouceSrcDir, tempResouceDesDir);
			}
			else{
				File tempResouceDesFile = null;
				if (deployResource.isSource() && compilePath.indexOf("/module/") > -1){
					String moduleName = parseModuleName(compilePath);
					if (compilePath.endsWith(".xml")){
						String fileName = this.parseXmlFileName(compilePath);
						if (compilePath.indexOf("/sqlmap/") > -1){
							compilePath = "/WEB-INF/classes/sqlmap/"+fileName;
						}else{
							compilePath = "/WEB-INF/modules/"+moduleName+"/"+fileName;							
						}
					}else{
						compilePath = compilePath.replaceAll("/classes/","/modules/"+moduleName+"/");						
					}
					tempResouceDesFile = new File(tempDir + compilePath);
				}else{
					tempResouceDesFile = new File(tempDir + compilePath);
				}
				File tempResouceSrcFile = new File(currentPath);
				FileUtil.copyFile(tempResouceSrcFile, tempResouceDesFile);
			}
		}
		
		ConsoleHandler.info("do zip files ...");
		
		ZipHelper zipUtil = new ZipHelper();
		String tempZipFile = tempDeployDir + "/" + appName + ".zip";
		zipUtil.doZip(tempDirFile.getAbsolutePath(),tempZipFile);
		
		DataSource source = new FileDataSource(new File(tempZipFile));
		deployableZip.setDataHandler(new DataHandler(source));
		
		return deployableZip;
	}
	
	private List<DeployResource> buildApplicaitonInputList(IJavaProject javaProject){
		List<DeployResource> result = new ArrayList<DeployResource>();
		String appName = javaProject.getProject().getName();
		DeployResource deployResource = new DeployResource();
		deployResource.setFileName(appName);
		deployResource.setFileType(FileTypes.Applicaiton);
		result.add(deployResource);
		
		this.selectedResourceList.add(deployResource);
		
		return result;
	}

	private List<DeployResource> buildModulesInputList(IJavaProject javaProject){
		List<DeployResource> result = new ArrayList<DeployResource>();
		for (int i=0;i < moduleNames.size();i++){
			String moduleName = moduleNames.get(i);
			DeployResource deployResource = new DeployResource();
			deployResource.setFileName(moduleName);
			deployResource.setFileType(FileTypes.Module);
			
			if (this.selectedModuleNames.contains(moduleName)){
				this.selectedResourceList.add(deployResource);
			}
			result.add(deployResource);
		}
		return result;
	}
	
	private String parseModuleName(String path){
		String result = null;
		int index = path.indexOf("/module/");
		int endIndex = path.indexOf("/", index+8);
		if (endIndex < 0){
			endIndex = path.length();
		}
		result = path.substring(index+8,endIndex);
		return result;
	}
	
	private List<DeployResource> buildAppFilesInputList(List<?> resources,IJavaProject javaProject){
		List<DeployResource> result = new ArrayList<DeployResource>();
		String appName = javaProject.getProject().getName();

		String srcFilesPrefix = "/"+appName+"/src";
		String webRootFilesPefix = "/"+appName+"/WebRoot";
		
		for (int i=0;i < resources.size();i++){
			Object object = resources.get(i);
			if (object instanceof IProject || object instanceof IJavaProject){
				continue;
			}
			if (object instanceof IResource){
				if (object instanceof IFolder){
					IResource folder = (IResource)object;	
					String folderName = folder.getName();
					if ("WebRoot".equals(folderName)){
						continue;
					}
					String folderPath = folder.getFullPath().toString();
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(folderName);
					deployResource.setFileType(FileTypes.Folder);
					deployResource.setFilePath(folderPath);
					deployResource.setCompilePath(folderPath.substring(webRootFilesPefix.length()));
					
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}else{
					IResource resource = (IResource)object;
					String resPath = resource.getFullPath().toString();
					
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(resource.getName());
					deployResource.setFileType(resource.getFileExtension());
					deployResource.setFilePath(resPath);

					if (resPath.startsWith(srcFilesPrefix)){
						String tempPath = resPath.substring(srcFilesPrefix.length());
						String compilePath = "/WEB-INF/classes"+tempPath;
						deployResource.setCompilePath(compilePath);						
						deployResource.setSource(true);
					}else{
						deployResource.setCompilePath(resPath.substring(webRootFilesPefix.length()));	
					}
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}
			}
			else if (object instanceof IJavaElement){
				if (object instanceof IPackageFragment){
					IPackageFragment javaPackage = (IPackageFragment)object;
					String packagePath = javaPackage.getPath().toString();
					
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(javaPackage.getElementName());
					deployResource.setFileType(FileTypes.Package);
					deployResource.setFilePath(packagePath);
					deployResource.setSource(true);
					
					String tempPath = packagePath.substring(srcFilesPrefix.length());
					String comilePath = "/WEB-INF/classes"+tempPath;
					deployResource.setCompilePath(comilePath);
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}else{
					IJavaElement javaElement = (IJavaElement)object;
					String elementName = javaElement.getElementName();
					if ("src".equals(elementName)){
						continue;
					}
					String resPath = javaElement.getPath().toString();
					DeployResource deployResource = new DeployResource();
					deployResource.setFileName(elementName);
					deployResource.setFileType(FileTypes.Java);
					deployResource.setFilePath(resPath);
					deployResource.setSource(true);
					
					String tempPath = resPath.substring(srcFilesPrefix.length());
					String comilePath = "/WEB-INF/classes"+tempPath;
					if (comilePath.endsWith(".java")){
						comilePath = comilePath.replaceAll(".java", ".class");						
					}
					deployResource.setCompilePath(comilePath);						
					result.add(deployResource);
					this.selectedResourceList.add(deployResource);
				}
			}
		}
		return result;
	}
	
	private IJavaProject retriveJavaProject(Object object){
		IJavaProject result = null;
		if (object instanceof IResource){
			IResource resource = (IResource)object;
			IProject project = resource.getProject();
			result = JavaModelUtil.getJavaProjectFromProject(project);
		}
		else if (object instanceof IJavaElement){
			IJavaElement javaElement = (IJavaElement)object;
			result = javaElement.getJavaProject();
		}
		return result;
	}
	
	private void parseModuleNames(IJavaProject javaProject){
		try {
			String appName = javaProject.getProject().getName();
			ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
			String mainPkg = projectConfig.getMainPkg();
			String mainPkgExt = mainPkg.replaceAll("\\.", "/");
			IPath path = javaProject.getProject().getLocation().append("/src/"+mainPkgExt+"/module");
			File file = path.toFile();
			if (file != null){
				IPath projectPath = javaProject.getProject().getFullPath();
				
				String[] childFiles = file.list();
				if (childFiles != null){
					for (int i=0;i < childFiles.length;i++){
						String moduleName = childFiles[i];
						this.moduleNames.add(moduleName);
						
						IPath tempModuleJavaPath = projectPath.append("/src/"+mainPkgExt+"/module/"+moduleName);
						this.moduleJavaPathMap.put(tempModuleJavaPath.toString(),moduleName);
						
						IPath tempModuleJspPath = projectPath.append("/WebRoot/jsp/"+moduleName);
						this.moduleJspPathMap.put(tempModuleJspPath.toString(),moduleName);
					}
				}
			}
		} catch (Exception e) {
			ConsoleHandler.error(e.getLocalizedMessage());
		}
	}
	
	private void deployApplication(final IJavaProject javaProject){
		Shell shell = targetPart.getSite().getShell();
		DeployDialog deployDialog = new DeployDialog(shell);
		
		List<DeployResource> inputList = this.buildApplicaitonInputList(javaProject);
		deployDialog.setInputDeployFilesList(inputList);
		deployDialog.setDeployType(DeployTypes.Application);
		deployDialog.setSelectedDeployFilesList(selectedResourceList);
		deployDialog.open();
		final boolean needReload = deployDialog.isNeedReload();
		
		if (deployDialog.getReturnCode() == Dialog.OK){
			final String appName = javaProject.getProject().getName();
			IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
    		Job deployJob = new Job("deployApp") {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						ConsoleHandler.info("prepare deploy " + appName + " ...");
						
						DeployableZip appZip = buildApplicaitonZip(javaProject,appName,needReload);
						ConsoleHandler.info("start deploy " + appName + " ...");
						
						ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
						String serverAddress = projectConfig.getServerAddress();
						String serverPort = projectConfig.getServerPort();
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(serverAddress, serverPort);
						hotServerManage.deployApplication(appName, appZip);
						
						ConsoleHandler.info("deploy " + appName + " successfully !");		
					} catch (Exception e) {
						ConsoleHandler.error(e.getLocalizedMessage());
					}
					return Status.OK_STATUS;
				}
			};
    		siteService.schedule(deployJob,0, true);
		}
	}
	
	private void deployModules(List<?> resources,final IJavaProject javaProject){
		Shell shell = targetPart.getSite().getShell();
		DeployDialog deployDialog = new DeployDialog(shell);
		
		List<DeployResource> inputList = buildModulesInputList(javaProject);
		deployDialog.setInputDeployFilesList(inputList);
		deployDialog.setDeployType(DeployTypes.Modules);
		deployDialog.setSelectedDeployFilesList(selectedResourceList);
		deployDialog.open();
		final boolean needReload = deployDialog.isNeedReload();
		
		if (deployDialog.getReturnCode() == Dialog.OK){
			final List<DeployResource> selectedDeployFilesList = deployDialog.getSelectedDeployFilesList();
			
			IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
    		Job deployJob = new Job("deployApp") {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						List<String> selectedModuleNames = new ArrayList<String>();
						for (int i=0;i < selectedDeployFilesList.size();i++){
							DeployResource deployResource = selectedDeployFilesList.get(i);
							String moduleName = deployResource.getFileName();
							selectedModuleNames.add(moduleName);
						}
						String appName = javaProject.getProject().getName();
						ConsoleHandler.info("prepare deploy modules ...");
						
						DeployableZip modulesZip = buildModulesZip(javaProject,appName,selectedModuleNames,needReload);
						
						ConsoleHandler.info("start deploy modules ...");
						
						ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
						String serverAddress = projectConfig.getServerAddress();
						String serverPort = projectConfig.getServerPort();
						
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(serverAddress, serverPort);
						hotServerManage.deployModules(appName,selectedModuleNames, modulesZip);	
						
						ConsoleHandler.info("deploy modules successfully !");	
					} catch (Exception e) {
						ConsoleHandler.error(e.getLocalizedMessage());
					}
					return Status.OK_STATUS;
				}
    		};
    		siteService.schedule(deployJob,0, true);
		}
	}	
	
	private void deployAppFiles(List<?> resources,final IJavaProject javaProject){
		DeployDialog deployDialog = new DeployDialog(targetPart.getSite().getShell());
		
		List<DeployResource> inputList = buildAppFilesInputList(resources,javaProject);
		boolean containReloadResouce = false;
		for (int i=0;i < inputList.size();i++){
			DeployResource deployResource = inputList.get(i);
			if (deployResource.isSource()){
				containReloadResouce = true;
				break;
			}
		}
		deployDialog.setContainReloadResouce(containReloadResouce);
		deployDialog.setInputDeployFilesList(inputList);
		deployDialog.setDeployType(DeployTypes.AppFiles);
		deployDialog.setSelectedDeployFilesList(selectedResourceList);
		deployDialog.open();
		
		final boolean needReload = deployDialog.isNeedReload();
		if (deployDialog.getReturnCode() == Dialog.OK){
			final List<DeployResource> selectedDeployFilesList = deployDialog.getSelectedDeployFilesList();
			for (int i=0;i < selectedDeployFilesList.size();i++){
				DeployResource deployResource = selectedDeployFilesList.get(i);
				if (FileTypes.Package.equals(deployResource.getFileType())){
					this.packagePathList.add(deployResource.getCompilePath());
				}
				else if (FileTypes.Folder.equals(deployResource.getFileType())){
					this.folderPathList.add(deployResource.getCompilePath());
				}
			}
			
			IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
    		Job deployJob = new Job("deployAppFiles") {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						String appName = javaProject.getProject().getName();
						ConsoleHandler.info("prepare deploy files ...");
						
						DeployableZip appFilesZip = buildAppFilesZip(javaProject,appName,selectedDeployFilesList,needReload);
						
						ConsoleHandler.info("start deploy files ...");
						ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
						String serverAddress = projectConfig.getServerAddress();
						String serverPort = projectConfig.getServerPort();
						
						for (int i=0;i < folderPathList.size();i++){
							deleteDirs.add(folderPathList.get(i));
						}
						for (int i=0;i < packagePathList.size();i++){
							String packagePath = packagePathList.get(i);
							if (packagePath.indexOf("/module/") > -1){
								if (packagePath.endsWith("sqlmap")){
									continue;
								}else{
									String moduleName = parseModuleName(packagePath);
									packagePath = packagePath.replaceAll("/classes/","/modules/"+moduleName);
									deleteDirs.add(packagePath);
								}
							}else{
								deleteDirs.add(packagePathList.get(i));								
							}
						}
						HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(serverAddress, serverPort);
						hotServerManage.deployAppFiles(appName,deleteDirs, appFilesZip);
						
						ConsoleHandler.info("deploy files successfully !");	
					} catch (Exception e) {
						ConsoleHandler.error(e.getLocalizedMessage());
					}
					return Status.OK_STATUS;
				}
    		};
    		siteService.schedule(deployJob,0, true);
		}
	}
	
	public void selectionChanged(IAction action, ISelection selection) {
		
	}

	@Override
	public void init(IViewPart viewPart) {
		this.targetPart = viewPart;  
	}
}