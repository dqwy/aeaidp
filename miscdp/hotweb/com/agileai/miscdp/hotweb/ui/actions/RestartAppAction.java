package com.agileai.miscdp.hotweb.ui.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.progress.IWorkbenchSiteProgressService;

import com.agileai.miscdp.hotweb.ConsoleHandler;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.util.MiscdpUtil;

public class RestartAppAction implements IObjectActionDelegate{
    private IWorkbenchPart targetPart;  
    
	public void run(IAction action) {
		ISelection selection = targetPart.getSite().getSelectionProvider().getSelection();
		IStructuredSelection curSelection = (IStructuredSelection)selection;
		IJavaProject javaProject = (IJavaProject)curSelection.getFirstElement();
		final String appName = javaProject.getProject().getName();
		
		IWorkbenchSiteProgressService siteService = (IWorkbenchSiteProgressService) targetPart.getSite().getAdapter(IWorkbenchSiteProgressService.class);     
		Job restartAppJob = new Job("restartApp") {
			@Override
			protected IStatus run(IProgressMonitor arg0) {
				ConsoleHandler.info("restarting application <" + appName + ">....");
				ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(appName);
				
				String serverAddress = projectConfig.getServerAddress();
				String serverPort = projectConfig.getServerPort();
				try {
					HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(serverAddress, serverPort);
					boolean result = hotServerManage.restartApplication(appName);
					if (result){
						ConsoleHandler.info("restart application <" + appName + "> sucessfully !");				
					}else{
						ConsoleHandler.info("restart application <" + appName + "> error !!");
					}
				} catch (Exception e) {
					ConsoleHandler.error(e.getLocalizedMessage());
				}
				return Status.OK_STATUS;
			}
			
		};
		siteService.schedule(restartAppJob,0, true);
	}

	public void setActivePart(IAction action, IWorkbenchPart workbenchPart) {
        this.targetPart = workbenchPart;  
	}
	public void selectionChanged(IAction action, ISelection selection) {
	}	
}