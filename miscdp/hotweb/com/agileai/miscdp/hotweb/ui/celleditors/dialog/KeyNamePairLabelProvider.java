﻿package com.agileai.miscdp.hotweb.ui.celleditors.dialog;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.agileai.domain.KeyNamePair;
/**
 * KeyName对标签提供器
 */
public class KeyNamePairLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof KeyNamePair) {
			KeyNamePair p = (KeyNamePair) element;
			if (columnIndex == 0) {
				return p.getKey();
			} else if (columnIndex == 1) {
				return p.getValue();
			} 
		}
		return null;
	}
	public String getText(Object element) {
		if (element instanceof KeyNamePair) {
			KeyNamePair p = (KeyNamePair) element;
			return p.getValue();
		}
		return null;
	}
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
