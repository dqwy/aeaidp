package com.agileai.miscdp.hotweb.ui.celleditors.dialog;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import com.agileai.domain.KeyNamePair;
import com.agileai.miscdp.hotweb.domain.CodeGroup;
import com.agileai.miscdp.hotweb.domain.CodeType;
/**
 * 提供者编辑器提示框
 */
public class ProviderDialog extends Dialog {
	private KeyNamePair keyNamePair = null;
	private String currentKey = null;
	private ListViewer listViewer;
	private ComboViewer comboViewer;
	private Combo combo;
	private List list;
	boolean cancelEdit = true;
	
	private String projectName = null;

	public ProviderDialog(Shell parentShell,String projectName) {
		super(parentShell);
		this.projectName = projectName;
	}

	@SuppressWarnings({"rawtypes" })
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout());

		final Label label = new Label(container, SWT.NONE);
		label.setText("请选择下拉框类型：");

		comboViewer = new ComboViewer(container, SWT.BORDER);
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				StructuredSelection selection = (StructuredSelection)comboViewer.getSelection();
				KeyNamePair keyNamePair = (KeyNamePair)selection.getFirstElement();
				listViewer.setInput(CodeType.prepareInput(projectName,keyNamePair.getKey()));
				listViewer.refresh();
			}
		});
		combo = comboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		comboViewer.setLabelProvider(new KeyNamePairLabelProvider());
		comboViewer.setContentProvider(new ArrayContentProvider());
		comboViewer.setInput(CodeGroup.prepareInput());			
		
		listViewer = new ListViewer(container, SWT.V_SCROLL | SWT.BORDER);
		listViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				buttonPressed(IDialogConstants.OK_ID);
			}
		});
		list = listViewer.getList();
		list.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				StructuredSelection selection = (StructuredSelection)listViewer.getSelection();
				keyNamePair = (KeyNamePair)selection.getFirstElement();
			}
		});
		list.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		listViewer.setLabelProvider(new KeyNamePairLabelProvider());
		listViewer.setContentProvider(new ArrayContentProvider());
		java.util.List inputList = CodeType.prepareInput(projectName,null);
		listViewer.setInput(inputList);
		listViewer.setSelection(new StructuredSelection(getSelectedList(inputList)));
		return container;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private	java.util.List getSelectedList(java.util.List inputList){
		java.util.List result = new ArrayList();
		if (currentKey != null){
			for (int i=0;i < inputList.size();i++){
				CodeType codeType = (CodeType)inputList.get(i);
				if (codeType.getKey().equals(currentKey)){
					result.add(codeType);
					break;
				}
			}
		}
		return result;
	}
	public void setCurrentKey(String currentKey) {
		this.currentKey = currentKey;
	}
	public boolean isCancelEdit() {
		return cancelEdit;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	@Override
	protected Point getInitialSize() {
		return new Point(290, 400);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("下拉框选项选择框");
	}
	public KeyNamePair getKeyNamePair(){
		return this.keyNamePair;
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			cancelEdit = false;
		}
		if (buttonId == IDialogConstants.CANCEL_ID) {
			this.keyNamePair = null;
		}
		super.buttonPressed(buttonId);
	}
}
