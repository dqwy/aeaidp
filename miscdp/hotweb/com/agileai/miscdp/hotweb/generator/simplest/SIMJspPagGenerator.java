﻿package com.agileai.miscdp.hotweb.generator.simplest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.generator.Generator;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
/**
 * 最简功能模型JSP代码生成器
 */
public class SIMJspPagGenerator implements Generator{
	private String defaultJspFile = null;
	private File xmlFile = null;
	private String charencoding = "UTF-8";
	private String templateDir = null;
	private String template = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
        	Configuration cfg = new Configuration();
        	cfg.setEncoding(Locale.getDefault(), charencoding);
        	cfg.setDirectoryForTemplateLoading(new File(templateDir));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
        	String templateFile = "/simplest/DefaultPage.jsp.ftl";
        	
        	if ("Html5View".equals(this.template)){
        		templateFile = "/simplest/MobileView.jsp.ftl";
        	}
        	else if ("Html5Form".equals(this.template)){
        		templateFile = "/simplest/MobileForm.jsp.ftl";
        	}
        	
        	Template temp = cfg.getTemplate(templateFile,charencoding);
        	temp.setEncoding(charencoding);
            Map root = new HashMap();
            NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
            root.put("doc",nodeModel);
            File file = new File(defaultJspFile);
            File parentFile = file.getParentFile();
            if (!parentFile.exists()){
            	parentFile.mkdirs();
            }
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charencoding));
//            Writer out = new OutputStreamWriter(System.out);
            temp.process(root, out);
            out.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}



	public String getDefaultJspFile() {
		return defaultJspFile;
	}

	public void setDefaultJspFile(String defaultJspFile) {
		this.defaultJspFile = defaultJspFile;
	}
	public File getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
}
