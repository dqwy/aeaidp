﻿package com.agileai.miscdp.hotweb.generator.treemanage;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.treemanage.TreeManageFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * Handler配置代码生成器
 */
public class TMMHandlerCfgGenerator implements Generator{
	private TreeManageFuncModel suFuncModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        String listHandlerId = suFuncModel.getHandlerId();
	        String listHandlerClass = suFuncModel.getHandlerClass();
	        String listJspPage = suFuncModel.getJspName();
	        MiscdpUtil.newHandlerConfigElement(document, listHandlerId, listHandlerClass, listJspPage);
	        
	        String parentJspName = listJspPage.substring(0,listJspPage.length()-14)+"ParentSelect"+".jsp";
	        MiscdpUtil.newHandlerConfigElement(document, listHandlerId.substring(0,listHandlerId.length()-10)+"ParentSelect", listHandlerClass.substring(0,listHandlerClass.length()-17)+"ParentSelectHandler", parentJspName);
	        
	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(TreeManageFuncModel suFuncModel) {
		this.suFuncModel = suFuncModel;
	}
}
