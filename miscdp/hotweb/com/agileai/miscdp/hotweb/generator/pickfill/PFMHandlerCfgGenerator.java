﻿package com.agileai.miscdp.hotweb.generator.pickfill;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.pickfill.PickFillFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * Handler配置代码生成器
 */
public class PFMHandlerCfgGenerator implements Generator{
	private PickFillFuncModel pickFillFuncModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        String listHandlerId = pickFillFuncModel.getListHandlerId();
	        String listHandlerClass = pickFillFuncModel.getListHandlerClass();
	        String listJspName = pickFillFuncModel.getListJspName();
	        MiscdpUtil.newHandlerConfigElement(document,listHandlerId,listHandlerClass,listJspName);
	        
	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(PickFillFuncModel suFuncModel) {
		this.pickFillFuncModel = suFuncModel;
	}
}
