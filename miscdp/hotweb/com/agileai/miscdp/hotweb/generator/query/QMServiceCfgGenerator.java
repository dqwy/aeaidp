﻿package com.agileai.miscdp.hotweb.generator.query;

import java.io.File;
import java.util.HashMap;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.query.QueryFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.util.XmlUtil;
/**
 * 服务配置代码生成器
 */
public class QMServiceCfgGenerator implements Generator{
	private QueryFuncModel queryModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        String serviceId = queryModel.getServiceId();
	        String serviceTargetId = serviceId.substring(0,serviceId.length()-7)+"Target";
	        
	        HashMap xmlMap = new HashMap();
	        xmlMap.put("s", "http://www.springframework.org/schema/beans");
	        XPath serviceTargetPath = document.createXPath("//s:beans/s:bean[@id='"+serviceTargetId+"']");
	        serviceTargetPath.setNamespaceURIs(xmlMap);
	        Node serviceTargetNode = serviceTargetPath.selectSingleNode(document);
	        if (serviceTargetNode != null){
	        	serviceTargetNode.getParent().remove(serviceTargetNode);
	        }
	        Element beansElement = (Element)document.selectSingleNode("//beans");
	        Element newServiceTarget = beansElement.addElement("bean");
	        String implClassName = queryModel.getImplClassName();
	        newServiceTarget.addAttribute("id",serviceTargetId);
	        newServiceTarget.addAttribute("class",implClassName);
	        newServiceTarget.addAttribute("parent","baseService");
	        String sqlNameSpace = queryModel.getSqlNameSpace();
	        newServiceTarget.addElement("property").addAttribute("name","sqlNameSpace")
	        	.addAttribute("value",sqlNameSpace);
        	
	        XPath servicePath = document.createXPath("//s:beans/s:bean[@id='"+serviceId+"']");
	        servicePath.setNamespaceURIs(xmlMap);
	        Node serviceNode = servicePath.selectSingleNode(document);
	        if (serviceNode != null){
	        	serviceNode.getParent().remove(serviceNode);
	        }
	        Element newService = beansElement.addElement("bean");
	        newService.addAttribute("id",serviceId);
	        newService.addAttribute("parent","transactionBase");
	        newService.addElement("property").addAttribute("name","target")
	        	.addAttribute("ref",serviceTargetId);
	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(QueryFuncModel suFuncModel) {
		this.queryModel = suFuncModel;
	}
}
