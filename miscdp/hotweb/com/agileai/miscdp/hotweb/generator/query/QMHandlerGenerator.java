﻿package com.agileai.miscdp.hotweb.generator.query;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.xml.sax.SAXException;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.FileUtil;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
/**
 * Handler代码生成器
 */
public class QMHandlerGenerator implements Generator{
	private String charencoding = "UTF-8";
	private String templateDir = null;
	private File xmlFile = null;
	private String srcPath = null;
	private String listHandlerClassName;
	private String detailHandlerClassName;
	
	private boolean showDetail = true;
	public void setShowDetail(boolean showDetail) {
		this.showDetail = showDetail;
	}
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
        	String listTemplateFile = "/query/ListHandler.java.ftl";
        	generateJavaFile(listTemplateFile,listHandlerClassName);
        	
        	if (showDetail){
        		String editTemplateFile = "/query/DetailHandler.java.ftl";
            	generateJavaFile(editTemplateFile,detailHandlerClassName);        		
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }	
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void generateJavaFile(String templateFile,String fullClassName) throws IOException, 
		SAXException, ParserConfigurationException, TemplateException{
        try {
        	Configuration cfg = new Configuration();
        	cfg.setEncoding(Locale.getDefault(), charencoding);
        	cfg.setDirectoryForTemplateLoading(new File(templateDir));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
        	Template temp = cfg.getTemplate(templateFile,charencoding);
        	temp.setEncoding(charencoding);
            Map root = new HashMap();
            NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
            root.put("doc",nodeModel);
            File javaFile = FileUtil.createJavaFile(srcPath, fullClassName);
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(javaFile),charencoding));
//            Writer out = new OutputStreamWriter(System.out);
            temp.process(root, out);
            out.flush();
            
            MiscdpUtil.getJavaFormater().format(javaFile.getAbsoluteFile(), charencoding);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
	public String getSrcPath() {
		return srcPath;
	}
	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}
	public void setListHandlerClassName(String listHandlerClassName) {
		this.listHandlerClassName = listHandlerClassName;
	}
	public void setDetailHandlerClassName(String detailHandlerClassName) {
		this.detailHandlerClassName = detailHandlerClassName;
	}
}
