﻿package com.agileai.miscdp.hotweb.generator;
/**
 * 代码生成器接口
 */
public interface Generator {
	void generate();
}
