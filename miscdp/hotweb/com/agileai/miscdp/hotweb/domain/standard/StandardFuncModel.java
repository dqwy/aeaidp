package com.agileai.miscdp.hotweb.domain.standard;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.PageParameter;
import com.agileai.hotweb.model.standard.StandardFuncModelDocument;
/**
 * 单表操作功能模型
 */
public class StandardFuncModel extends BaseFuncModel{
	protected String tableName = "";
	protected String pkGenPolicy = "";
	protected String detailJspName = "";
	protected String listTitle = "";
	protected String detailTitle = "";
	protected List<String> rsIdColumns = new ArrayList<String>();
	protected boolean exportCsv = true;
	protected boolean exportXls = true;
	protected boolean sortAble = true;
	protected boolean pagination = true;
	protected String listHandlerId="";
	protected String editHandlerId="";
	protected String listHandlerClass="";
	protected String editHandlerClass="";
	
	protected List<PageParameter> pageParameters = new ArrayList<PageParameter>();
	protected List<ListTabColumn> listTableColumns = new ArrayList<ListTabColumn>();
	protected List<PageFormField> pageFormFields = new ArrayList<PageFormField>();
	
	public StandardFuncModel(){
		this.editorId = DeveloperConst.SU_EDITOR_ID;
	}
	public StandardFuncModel(String funcModelDefin){
		
	}
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String table) {
		this.tableName = table;
	}
	public String getDetailJspName() {
		return detailJspName;
	}
	public void setDetailJspName(String detailJspName) {
		if (detailJspName != null){
			this.detailJspName = detailJspName;			
		}
	}
	public String getListTitle() {
		return listTitle;
	}
	public void setListTitle(String listTitle) {
		this.listTitle = listTitle;
	}
	public String getDetailTitle() {
		return detailTitle;
	}
	public void setDetailTitle(String detailTitle) {
		this.detailTitle = detailTitle;
	}
	public List<ListTabColumn> getListTableColumns() {
		return listTableColumns;
	}
	public List<PageParameter> getPageParameters() {
		return pageParameters;
	}
	public List<PageFormField> getPageFormFields() {
		return pageFormFields;
	}
	public boolean isExportCsv() {
		return exportCsv;
	}
	public void setExportCsv(boolean exportCsv) {
		this.exportCsv = exportCsv;
	}
	public boolean isExportXls() {
		return exportXls;
	}
	public void setExportXls(boolean exportXls) {
		this.exportXls = exportXls;
	}
	public boolean isSortAble() {
		return sortAble;
	}
	public void setSortAble(boolean sortAble) {
		this.sortAble = sortAble;
	}
	public boolean isPagination() {
		return pagination;
	}
	public void setPagination(boolean pagination) {
		this.pagination = pagination;
	}
	public List<String> getRsIdColumns() {
		return rsIdColumns;
	}
	
	public void buildFuncModel(String funcDefine){
		StandardFuncModel standardFuncModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			StandardFuncModelDocument funcModelDocument = StandardFuncModelDocument.Factory.parse(reader);
			
			StandardFuncModelDocument.StandardFuncModel.BaseInfo baseInfo = funcModelDocument.getStandardFuncModel().getBaseInfo();
			standardFuncModel.setTableName(baseInfo.getTableName());
			standardFuncModel.setPkGenPolicy(baseInfo.getPkGenPolicy());
			standardFuncModel.setListHandlerId(baseInfo.getHandler().getListHandlerId());
			standardFuncModel.setListHandlerClass(baseInfo.getHandler().getListHandlerClass());
			standardFuncModel.setEditHandlerId(baseInfo.getHandler().getEditHandlerId());
			standardFuncModel.setEditHandlerClass(baseInfo.getHandler().getEditHandlerClass());
			
			standardFuncModel.setServiceId(baseInfo.getService().getServiceId());
			standardFuncModel.setImplClassName(baseInfo.getService().getImplClassName());
			standardFuncModel.setInterfaceName(baseInfo.getService().getInterfaceName());
			standardFuncModel.setListJspName(baseInfo.getListJspName());
			standardFuncModel.setDetailJspName(baseInfo.getDetailJspName());
			standardFuncModel.setListTitle(baseInfo.getListTitle());
			standardFuncModel.setDetailTitle(baseInfo.getDetailTitle());
			standardFuncModel.setTemplate(baseInfo.getTemplate());
            standardFuncModel.setListSql(baseInfo.getQueryListSql());
            
            StandardFuncModelDocument.StandardFuncModel.ListView listView = funcModelDocument.getStandardFuncModel().getListView();
			String rsIdColumnTemp  = listView.getListTableArea().getRow().getRsIdColumn();
			String[] rsIdColumnArray = rsIdColumnTemp.split(",");

			standardFuncModel.getRsIdColumns().clear();
			for (int i=0;i < rsIdColumnArray.length;i++){
				standardFuncModel.getRsIdColumns().add(rsIdColumnArray[i]);
			}
			com.agileai.hotweb.model.FormObjectDocument.FormObject[] formObjects = listView.getParameterArea().getFormObjectArray();
			processPageParamters(formObjects, pageParameters);
			
			com.agileai.hotweb.model.ListTableType listTableType = listView.getListTableArea();
			standardFuncModel.setExportCsv(listTableType.getExportCsv());
			standardFuncModel.setExportXls(listTableType.getExportXls());
			standardFuncModel.setSortAble(listTableType.getSortAble());
			standardFuncModel.setPagination(listTableType.getPagination());

			com.agileai.hotweb.model.ListTableType.Row.Column columns[] = listTableType.getRow().getColumnArray();
			processListTableColumns(columns, listTableColumns);
			
			StandardFuncModelDocument.StandardFuncModel.DetailView detailView = funcModelDocument.getStandardFuncModel().getDetailView();
			com.agileai.hotweb.model.FormObjectDocument.FormObject[] editFormObjects =detailView.getDetailEditArea().getFormObjectArray();
			processPageFormFields(editFormObjects, pageFormFields);
			
			processResFileValueProvider(editFormObjects);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getListHandlerId() {
		return listHandlerId;
	}
	public void setListHandlerId(String listHandlerId) {
		this.listHandlerId = listHandlerId;
	}
	public String getEditHandlerId() {
		return editHandlerId;
	}
	public void setEditHandlerId(String editHandlerId) {
		this.editHandlerId = editHandlerId;
	}
	public String getListHandlerClass() {
		return listHandlerClass;
	}
	public void setListHandlerClass(String listHandlerClass) {
		this.listHandlerClass = listHandlerClass;
	}
	public String getEditHandlerClass() {
		return editHandlerClass;
	}
	public void setEditHandlerClass(String editHandlerClass) {
		this.editHandlerClass = editHandlerClass;
	}
	public String getPkGenPolicy() {
		return pkGenPolicy;
	}
	public void setPkGenPolicy(String pkGenPolicy) {
		this.pkGenPolicy = pkGenPolicy;
	}
	public String getPrimaryKey(){
		String result = "";
		for (int i=0;i < pageFormFields.size();i++){
			PageFormField pageFormField = pageFormFields.get(i);
			if ("Y".equals(pageFormField.getIsPK())){
				result = pageFormField.getCode();
			}
		}
		return result;
	}
}
