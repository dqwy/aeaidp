package com.agileai.miscdp.hotweb.domain.mastersub;

public class SubTableInfo {
	private String subTableId = null;
	private boolean isEntryEdit = true;
	private String findSubRecordsSql = null;
	private String getSubRecordSql = null;
	private String deleteSubRecordsSql = null;
	private String deleteSubRecordSql = null;
	private String insertSubRecordSql = null;
	private String updateSubRecordSql = null;
	
	public String getDeleteSubRecordSql() {
		return deleteSubRecordSql;
	}
	public void setDeleteSubRecordSql(String deleteSubRecordSql) {
		this.deleteSubRecordSql = deleteSubRecordSql;
	}
	public String getDeleteSubRecordsSql() {
		return deleteSubRecordsSql;
	}
	public void setDeleteSubRecordsSql(String deleteSubRecordsSql) {
		this.deleteSubRecordsSql = deleteSubRecordsSql;
	}
	public String getFindSubRecordsSql() {
		return findSubRecordsSql;
	}
	public void setFindSubRecordsSql(String findSubRecordsSql) {
		this.findSubRecordsSql = findSubRecordsSql;
	}
	public String getGetSubRecordSql() {
		return getSubRecordSql;
	}
	public void setGetSubRecordSql(String getSubRecordSql) {
		this.getSubRecordSql = getSubRecordSql;
	}
	public String getInsertSubRecordSql() {
		return insertSubRecordSql;
	}
	public void setInsertSubRecordSql(String insertSubRecordSql) {
		this.insertSubRecordSql = insertSubRecordSql;
	}
	public String getSubTableId() {
		return subTableId;
	}
	public void setSubTableId(String subTableId) {
		this.subTableId = subTableId;
	}
	public String getUpdateSubRecordSql() {
		return updateSubRecordSql;
	}
	public void setUpdateSubRecordSql(String updateSubRecordSql) {
		this.updateSubRecordSql = updateSubRecordSql;
	}
	public boolean isEntryEdit() {
		return isEntryEdit;
	}
	public void setEntryEdit(boolean isEntryEdit) {
		this.isEntryEdit = isEntryEdit;
	}
}
