package com.agileai.miscdp.hotweb.domain;

public class ResFileValueProvider {
	public static final String FormIdValue = "__ResouceList";
	
	public static final String PrimaryKeyField = "REF_ID";
	public static final String BizIdField = "BIZ_ID";
	public static final String ResIdField = "RES_ID";
	
	public static final String TableNameKey = "tableName";
	public static final String PrimaryKeyFieldKey = "primaryKeyField";
	public static final String BizIdFieldKey = "bizIdField";
	public static final String ResIdFieldKey = "resIdField";
	
	private String tableName = null;
	private String primaryKeyField = null;
	private String bizIdField = null;
	private String resIdField = null;
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getPrimaryKeyField() {
		return primaryKeyField;
	}
	public void setPrimaryKeyField(String primaryKeyField) {
		this.primaryKeyField = primaryKeyField;
	}
	public String getBizIdField() {
		return bizIdField;
	}
	public void setBizIdField(String bizIdField) {
		this.bizIdField = bizIdField;
	}
	public String getResIdField() {
		return resIdField;
	}
	public void setResIdField(String resIdField) {
		this.resIdField = resIdField;
	}
}