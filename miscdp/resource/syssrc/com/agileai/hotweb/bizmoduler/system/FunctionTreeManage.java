package com.agileai.hotweb.bizmoduler.system;

import com.agileai.hotweb.bizmoduler.core.TreeManage;
import com.agileai.hotweb.bizmoduler.frame.FunctionManage;

public interface FunctionTreeManage
        extends TreeManage,FunctionManage {
}