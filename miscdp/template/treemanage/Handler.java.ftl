<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treemanage",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeManageFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local editArea = .node.PageView.CurrentEditArea>
<#local createArea = .node.PageView.ChildCreateArea>
package ${Util.parsePkg(handler.@handlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.TreeManageHandler;
import com.agileai.domain.*;
import com.agileai.util.*;
import com.agileai.hotweb.domain.*;

public class ${Util.parseClass(handler.@handlerClass)} extends TreeManageHandler{
	public ${Util.parseClass(handler.@handlerClass)}(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
		this.nodeIdField = "${baseInfo.@idField}";
		this.nodePIdField = "${baseInfo.@parentIdField}";
		this.moveUpErrorMsg = "该节点是第一个节点，不能上移！";
		this.moveDownErrorMsg = "该节点是最后一个节点，不能下移！";
		this.deleteErrorMsg = "该节点还有子节点，不能删除！";
	}
	protected TreeBuilder provideTreeBuilder(DataParam param){
		${Util.parseClass(service.@InterfaceName)} service = this.getService();
		List<DataRow> menuRecords = service.findTreeRecords(param);
		TreeBuilder treeBuilder = new TreeBuilder(menuRecords,"${baseInfo.@idField}","${baseInfo.@nameField}","${baseInfo.@parentIdField}");
		return treeBuilder;
	}
	protected void processPageAttributes(DataParam param) {
		<#compress>
		<#if Util.isValid(editArea)><#list editArea["fa:FormObject"] as formObject><@SetEditAttribute formAtom=formObject/>
		</#list></#if>
		<#if Util.isValid(createArea)><#list createArea["fa:FormObject"] as formObject><@SetCreateAttribute formAtom=formObject/>
		</#list></#if>
		</#compress>
	}
	protected String provideDefaultNodeId(DataParam param){
		return "00000000-0000-0000-00000000000000000";
	}
	protected boolean isRootNode(DataParam param){
		boolean result = true;
		String nodeId = param.get(this.nodeIdField,this.provideDefaultNodeId(param));
		DataParam queryParam = new DataParam(this.nodeIdField,nodeId);
		DataRow row = this.getService().queryCurrentRecord(queryParam);
		if (row == null){
			result = false;
		}else{
			String parentId = row.stringValue("${baseInfo.@parentIdField}");
			result = StringUtil.isNullOrEmpty(parentId);
		}
		return result;
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>
<#macro SetEditAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getAttributeValue("${atom["fa:Name"]}")));</#if>
</#macro>
<#macro SetCreateAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getAttributeValue("${atom["fa:Name"]}","${atom["fa:DefValue"].@value}")));</#if>
</#macro>