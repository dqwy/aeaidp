<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/standard",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro StandardFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local detailArea = .node.DetailView.DetailEditArea>
<#local paramArea = .node.ListView.ParameterArea>
<#local listTableArea = .node.ListView.ListTableArea>
package ${Util.parsePkg(handler.@listHandlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.domain.*;
import com.agileai.util.*;
import com.agileai.hotweb.domain.*;

public class ${Util.parseClass(handler.@listHandlerClass)} extends StandardListHandler{
	public ${Util.parseClass(handler.@listHandlerClass)}(){
		super();
		this.editHandlerClazz = ${Util.parseClass(handler.@editHandlerClass)}.class;
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
	}
	protected void processPageAttributes(DataParam param) {
		<#compress>
		<#if Util.isValid(paramArea)>
			<#list paramArea["fa:FormObject"] as formObject><@SetAttribute formAtom=formObject/></#list>
		</#if>
		<#local row =listTableArea["fa:Row"][0]>
		<#list row["fa:Column"] as columnVar><@InitMappingItem column=columnVar></@InitMappingItem></#list>
		</#compress>
		
	}
	protected void initParameters(DataParam param) {
		<#compress>
		<#if Util.isValid(paramArea)>
		<#list paramArea["fa:FormObject"] as formAtom>
			<#local atom = formAtom.*[0]>
			<#if Util.isValid(atom["fa:DefValue"])>
				<#if Util.isFalse(atom["fa:DefValue"].@isVariable)>initParamItem(param,"${atom["fa:Name"]}","${atom["fa:DefValue"].@value}");<#else>initParamItem(param,"${atom["fa:Name"]}",${atom["fa:DefValue"].@value});
			</#if></#if>
		</#list>
		</#if>
		</#compress>
		
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(param.get("${atom["fa:Name"]}")));</#if>
</#macro>
<#macro InitMappingItem column>
	<#if Util.isValidValue(column.@mappingItem)>
	initMappingItem("${column.@property}",FormSelectFactory.create("${column.@mappingItem}").getContent());
	</#if>
</#macro>