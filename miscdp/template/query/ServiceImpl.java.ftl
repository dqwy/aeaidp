<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/query",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro QueryFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@ImplClassName)};

import com.agileai.hotweb.bizmoduler.core.QueryModelServiceImpl;
import ${service.@InterfaceName};

public class ${Util.parseClass(service.@ImplClassName)} extends QueryModelServiceImpl 
	implements ${Util.parseClass(service.@InterfaceName)} {
	
	public ${Util.parseClass(service.@ImplClassName)}(){
		super();
	}
}
</#macro>
