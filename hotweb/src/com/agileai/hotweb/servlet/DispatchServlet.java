package com.agileai.hotweb.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.frame.OnlineCounterService;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.ClassLoaderFactory;
import com.agileai.hotweb.common.Constants;
import com.agileai.hotweb.common.Constants.FrameHandlers;
import com.agileai.hotweb.common.HandlerParser;
import com.agileai.hotweb.common.HotwebAuthHelper;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.i18n.BundleContext;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

public class DispatchServlet extends HttpServlet {
	protected Logger log = Logger.getLogger(this.getClass());
	protected static HashMap<ClassLoader,List<String>> PublicHandlerIdListCache = new HashMap<ClassLoader,List<String>>();
	private static final long serialVersionUID = 1489970485928178334L;
	
	final public void init() throws ServletException {
		super.init();
		
		String appContextPath = this.getServletContext().getRealPath("/");
		if (!appContextPath.endsWith(java.io.File.separator)) {
			appContextPath = appContextPath + java.io.File.separator;
		}
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ClassLoaderFactory.initAppClassLoaderPathPair(classLoader, appContextPath);
		
		String serviceConfigLocation = this.getServletContext().getInitParameter(Constants.ConfigFile.SERVICE);
		if (!StringUtil.isNullOrEmpty(serviceConfigLocation)){
			BeanFactory.init(serviceConfigLocation);			
		}
		String handlerConfigLocation = this.getServletContext().getInitParameter(Constants.ConfigFile.HANDLER);
		if (!StringUtil.isNullOrEmpty(handlerConfigLocation)){
			HandlerParser.init(handlerConfigLocation);
		}
		String beanId = this.getServletContext().getInitParameter(Constants.OnlineCounterServiceKey);
		if (!StringUtil.isNullOrEmpty(beanId)){
			OnlineCounterService onlineCounterService = (OnlineCounterService)BeanFactory.instance().getBean(beanId);
			onlineCounterService.initOnlineCount();
		}
		
		List<String> publicHandlerIdList = PublicHandlerIdListCache.get(classLoader);
		String publicHandlerIds = this.getInitParameter(Constants.PublicHandlerIdsKey);
		if (publicHandlerIdList == null){
			publicHandlerIdList = new ArrayList<String>();
			PublicHandlerIdListCache.put(classLoader, publicHandlerIdList);
			if (!StringUtil.isNullOrEmpty(publicHandlerIds)){
				String[] publicHandlerIdArray = publicHandlerIds.split(",");
				ListUtil.addArrayToList(publicHandlerIdList, publicHandlerIdArray);
			}
		}
		this.initResource();
	}
	final public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		this.process(request, response);
	}
	final public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		this.process(request, response);
	}
	protected void initResource(){
		String localeExtNames = this.getInitParameter("localeExtNames");
		if (!StringUtil.isNullOrEmpty(localeExtNames)){
			String[] localeExtNameArray = localeExtNames.split(",");
			List<String> localeExtNameList = new ArrayList<String>();
			ListUtil.addArrayToList(localeExtNameList, localeExtNameArray);
			BundleContext.getOnly().init(localeExtNameList);
		}
	}
	
	protected void process(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException{
		log.debug("DispatchServlet------start");
		try {
			ViewRenderer viewRenderer = null;
			String handlerId = parseHandlerId(request);
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			List<String> publicHandlerIdList = PublicHandlerIdListCache.get(classLoader);
			if (isExpired(request) 
					&& !FrameHandlers.LoginHandlerId.equals(handlerId)
					&& !publicHandlerIdList.contains(handlerId)){
				if (!Constants.FrameHandlers.MenuTreeHandlerId.equals(handlerId)){
					viewRenderer = new RedirectRenderer(request.getContextPath());	
				}
				else{
					StringBuffer responseText = new StringBuffer();
					String contextPath = request.getContextPath();
					responseText.append("parent.location.href='").append(contextPath).append("'");
					viewRenderer = new AjaxRenderer(responseText.toString());					
				}
			}
			else{
				processFunctionId(request);
				
				BaseHandler handler = null;
				List<String> fullHandlerList = this.parseFullHandler(handlerId);
				if (fullHandlerList.size() > 1){
					String moduleName = fullHandlerList.get(0);
					String realHandlerId = fullHandlerList.get(1);
					ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(classLoader);
					ClassLoader moduleClassLoader = classLoaderFactory.createModuleClassLoader(moduleName);
					handler = HandlerParser.getOnly(moduleClassLoader).instantiateHandler(realHandlerId);
					handler.setModuleName(moduleName);
					handler.setHandlerId(realHandlerId);
				}else{
					handler = HandlerParser.getOnly().instantiateHandler(handlerId);					
				}
				handler.setDispatchServlet(this);
				handler.setRequest(request);
				handler.setResponse(response);
				DataParam param = new DataParam(request.getParameterMap());
				viewRenderer = handler.processRequest(param);
				viewRenderer.setHandler(handler);
			}
			viewRenderer.executeRender(this, request, response);	
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("DispatchServlet------end");
	}
	
	private List<String> parseFullHandler(String handlerId){
		List<String> result = new ArrayList<String>();
		int dotIndex = handlerId.indexOf("."); 
		if (dotIndex > 0){
			String moduleName = handlerId.substring(0,dotIndex);
			String realHandlerId = handlerId.substring(dotIndex+1,handlerId.length());
			result.add(moduleName);
			result.add(realHandlerId);
		}else{
			result.add(handlerId);
		}
		return result;
	}
	
	private void processFunctionId(HttpServletRequest request){
		String currentFuncId = request.getParameter("__function_id__");
		User user = null;
		if (!StringUtil.isNullOrEmpty(currentFuncId)){
			HttpSession session = request.getSession(false);
			if (session != null){
				Profile profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);
				if (profile != null){
					user = (User)profile.getUser();				
				}
			} 
		}
		if (user != null){
			HotwebAuthHelper authHelper = new HotwebAuthHelper(user);
			authHelper.setCurrentFuncId(currentFuncId);
		}
	}
	
	protected boolean isExpired(HttpServletRequest request){
		boolean result = true;
		HttpSession session = request.getSession(false);
		if (session != null){
			Profile profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);
			if (profile != null){
				result = false;				
			}
		}
		return result;
	}

	protected String parseHandlerId(HttpServletRequest request) {
		String result = "";
		String queryString = request.getQueryString().trim();
		int firstIndex = queryString.indexOf("actionType=");
		if (firstIndex < 0) {
			int endIndex = queryString.length();
			int tempIndex = queryString.indexOf("&");
			if (tempIndex > -1) {
				endIndex = tempIndex;
			}
			result = queryString.substring(0, endIndex);
		} else {
			int lastIndex = queryString.lastIndexOf("actionType=");
			if (firstIndex == lastIndex) {
				int endIndex = queryString.length();
				int tempIndex = queryString.indexOf("&");
				if (tempIndex > -1) {
					endIndex = tempIndex;
				}
				result = queryString.substring(0, endIndex);
			} else {
				String temp = queryString.substring(0, lastIndex - 1);
				int index = temp.lastIndexOf("&");
				result = temp.substring(index + 1, temp.length());
			}
		}
		return result;
	}
}
