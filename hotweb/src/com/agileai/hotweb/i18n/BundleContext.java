package com.agileai.hotweb.i18n;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class BundleContext {
	private static BundleContext bundleContext = new BundleContext();
	protected static HashMap<ClassLoader,ResourceBundle> ResourceBundleCache = new HashMap<ClassLoader,ResourceBundle>();
	public static final String I18NPropertiesName = "i18n/resource/Locale_";
	
	private BundleContext(){
		
	}
	
	public static BundleContext getOnly(){
		return bundleContext;
	}
	
	public void init(List<String> localeExtNameList){
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		for (int i=0;i < localeExtNameList.size();i++){
			String localeExtName = localeExtNameList.get(i);
			String propertiesFileName = I18NPropertiesName + localeExtName + ".properties";
			InputStream inputStream = classLoader.getResourceAsStream(propertiesFileName);
			try {
				Properties prop = new Properties();
				prop.load(inputStream);
				if (inputStream != null) {
					inputStream.close();
				}
				ResourceBundle.PropertiesCache.put(localeExtName, prop);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public ResourceBundle getResourceBundle(){
		return new ResourceBundle();
	}
}