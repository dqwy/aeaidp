package com.agileai.hotweb.common;

public class ModuleManager {
	private ClassLoader appClassLoader = null;

	public ModuleManager(ClassLoader appClassLoader){
		this.appClassLoader = appClassLoader;
	}
	
	public boolean load(String moduleName){
		boolean result = false;
		ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
		classLoaderFactory.createModuleClassLoader(moduleName);
		result = true;
		return result;
	}
	
	public boolean unload(String moduleName){
		boolean result = false;
		ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
		ClassLoader moduleClassLoader = classLoaderFactory.rejectModuleClassLoader(moduleName);
		
		BeanFactory.destroy(moduleClassLoader);
		HandlerParser.destroy(moduleClassLoader);
		moduleClassLoader = null;

		System.gc();
		
		result = true;
		return result;
	}
	
	public boolean reload(String moduleName){
		boolean result = false;
		
		result = this.unload(moduleName);
		if (result){
			result = this.load(moduleName);			
		}
		return result;
	}	
}
