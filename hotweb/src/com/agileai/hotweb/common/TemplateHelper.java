package com.agileai.hotweb.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;

public class TemplateHelper {
	/**
	 * @param templateDir   eg: /opt/resources/templates or d:/resources/templates 
	 * @param templateFile  eg: /DayReport.ftl
	 * @param outputStream
	 * @param modelMap
	 */
	@SuppressWarnings("rawtypes")
	public void generate(String templateDir,String templateFile,OutputStream outputStream,Map modelMap) {
		generate(templateDir,templateFile,outputStream,modelMap, "utf-8");
	}	
	
	/**
	 * @param templateDir   eg: /opt/resources/templates or d:/resources/templates 
	 * @param templateFile  eg: /DayReport.ftl
	 * @param outputStream
	 * @param modelMap
	 * @param charencoding  eg: utf-8
	 */
	@SuppressWarnings("rawtypes")
	public void generate(String templateDir,String templateFile,OutputStream outputStream,Map modelMap,String charencoding) {
		try {
			Configuration cfg = new Configuration();
			cfg.setDirectoryForTemplateLoading(new File(templateDir));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			cfg.setEncoding(Locale.getDefault(), charencoding);
			Template temp = cfg.getTemplate(templateFile);
			temp.setEncoding(charencoding);
			
			Writer out = new BufferedWriter(new OutputStreamWriter(outputStream, charencoding));
			temp.process(modelMap, out);
			out.flush();
			IOUtils.closeQuietly(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void generate(String templateText,Map modelMap,OutputStream outputStream){
		generate(templateText,modelMap,outputStream,"utf-8");
	}
	
	@SuppressWarnings("rawtypes")
	public void generate(String templateText,Map modelMap,OutputStream outputStream,String charencoding){
		try {
			StringWriter writer = new StringWriter();
        	Configuration cfg = new Configuration();
        	cfg.setTemplateLoader(new StringTemplateLoader(templateText));  
        	cfg.setEncoding(Locale.getDefault(), charencoding);
            cfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
        	Template temp = cfg.getTemplate("");
        	temp.setEncoding(charencoding);
        	
			Writer out = new BufferedWriter(new OutputStreamWriter(outputStream,charencoding));
			temp.process(modelMap, out);
			
            writer.flush();
            IOUtils.closeQuietly(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
