package com.agileai.hotweb.bizmoduler.system;

import com.agileai.hotweb.bizmoduler.core.TreeSelectService;

public interface SecurityRoleTreeSelect
        extends TreeSelectService {
}
