package com.agileai.hotweb.database;

import java.io.Reader;

import com.agileai.domain.DataRowConvert;

public class OracleClobConverter implements DataRowConvert{
	@Override
	public Object convert(Object value) {
		if (value != null && (value instanceof oracle.sql.CLOB || value instanceof oracle.sql.NCLOB)){
			String content = null;
			StringBuffer stringBuf = new StringBuffer();
			try {
				Reader inStream = null;
				if (value instanceof oracle.sql.CLOB){
					oracle.sql.CLOB clob = (oracle.sql.CLOB)value;
					inStream = clob.getCharacterStream();					
				}else{
					oracle.sql.NCLOB clob = (oracle.sql.NCLOB)value;
					inStream = clob.getCharacterStream();		
				}
				int length = 0;
				char[] buffer = new char[10];
				while ((length = inStream.read(buffer)) != -1) {
					for (int i = 0; i < length; i++) {
						stringBuf.append(buffer[i]);
					}
				}
				inStream.close();
				content = stringBuf.toString();
			} catch (Exception ex) {
				System.out.println("DataRowConvert4Oracle10Clob.convert:" + ex.getMessage());
			}
			return content;
		}else{
			return value;
		}
	}
}
