package com.agileai.hotweb.controller.core;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.frame.SecurityAuthorizationConfig;
import com.agileai.hotweb.common.ModuleManager;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;

public class ModuleCycleHandler extends BaseHandler{
	
	@PageAction
	public ViewRenderer load(DataParam param){
		JSONObject response = new JSONObject();
		try {
			String moduleNames = param.get("moduleNames");
			if (StringUtil.isNotNullNotEmpty(moduleNames) && checkUserAuth()){
				String[] moduleNameArray = moduleNames.split(",");
				ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
				ModuleManager moduleManager = new ModuleManager(appClassLoader);
				
				boolean loadResult = true;
				for (int i=0;i < moduleNameArray.length;i++){
					String moduleName = moduleNameArray[i];
					loadResult = loadResult && moduleManager.load(moduleName);
				}
				if (loadResult){
					response.put("status", "OK");	
				}else{
					response.put("status", "Error");
					response.put("message", "load module " + moduleName + " failure !");
				}
			}else{
				response.put("status", "Error");
				response.put("message", "param or user is not valid !");
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(response.toString());
	}
	
	@PageAction
	public ViewRenderer unload(DataParam param){
		JSONObject response = new JSONObject();
		try {
			String moduleNames = param.get("moduleNames");
			if (StringUtil.isNotNullNotEmpty(moduleNames) && checkUserAuth()){
				String[] moduleNameArray = moduleNames.split(",");
				ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
				ModuleManager moduleManager = new ModuleManager(appClassLoader);
				
				boolean unloadResult = true;
				for (int i=0;i < moduleNameArray.length;i++){
					String moduleName = moduleNameArray[i];
					unloadResult = unloadResult && moduleManager.unload(moduleName);
				}
				if (unloadResult){
					response.put("status", "OK");	
				}else{
					response.put("status", "Error");
					response.put("message", "unload module " + moduleName + " failure !");
				}
			}else{
				response.put("status", "Error");
				response.put("message", "param or user is not valid !");
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(response.toString());
	}
	
	private boolean checkUserAuth() throws JSONException{
		boolean result = false;
		String inputString = this.getInputString();
		if (StringUtil.isNotNullNotEmpty(inputString)){
			JSONObject requestJson = new JSONObject(inputString);
			String userId = requestJson.getString("userId");
			String userPwd = requestJson.getString("userPwd");
			if (StringUtil.isNotNullNotEmpty(userId) && StringUtil.isNotNullNotEmpty(userPwd) && "admin".equals(userId)){
				SecurityAuthorizationConfig authorizationConfig = (SecurityAuthorizationConfig)this.lookupService("securityAuthorizationConfigService");
				DataRow userRow = authorizationConfig.retrieveUserRecord(userId);
				if (userRow != null && userRow.size() > 0){
					String userPwdTemp = String.valueOf(userRow.get("USER_PWD"));
					String encryptedPassword = CryptionUtil.md5Hex(userPwd);
					if (userPwdTemp.equals(encryptedPassword)){
						result = true;
					}
				}
			}
		}
		return result;
	}
}