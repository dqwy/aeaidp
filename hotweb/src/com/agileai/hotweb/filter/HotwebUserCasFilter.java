package com.agileai.hotweb.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jasig.cas.client.authentication.AttributePrincipal;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.frame.SecurityAuthorizationConfig;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.HotwebAuthHelper;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;

public class HotwebUserCasFilter implements Filter{
	
	@Override
	public void destroy() {
		
	}
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
	    HttpServletRequest httpRequest = (HttpServletRequest) request;
	    AttributePrincipal attributePrincipal =  (AttributePrincipal)httpRequest.getUserPrincipal();
	    if (attributePrincipal != null) {
	        String loginName = attributePrincipal.getName();
	        Profile profile = getProfile(httpRequest);
	        if (profile == null) {
				String fromIpAddress = request.getLocalAddr();
				User user = new User();
				user.setUserCode(loginName);
				
				profile = new Profile(loginName,fromIpAddress,user);
				HttpSession session = httpRequest.getSession(true);
				session.setAttribute(Profile.PROFILE_KEY, profile);
				
				BeanFactory beanFactory = BeanFactory.instance();
				SecurityAuthorizationConfig authorizationConfig = (SecurityAuthorizationConfig)beanFactory.getBean("securityAuthorizationConfigService");
				DataRow userRow = authorizationConfig.retrieveUserRecord(loginName);
				
				HotwebAuthHelper authHelper = new HotwebAuthHelper(user);
				authHelper.initAuthedUser(userRow,user);
	        }
	    }
	    chain.doFilter(request, response);		
	}
	
	protected Profile getProfile(HttpServletRequest request){
		Profile profile = null;
		HttpSession session = request.getSession(false);
		if (session != null){
			profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);	
		}
		return profile;
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
}
