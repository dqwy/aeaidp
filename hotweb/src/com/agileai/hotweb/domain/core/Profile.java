package com.agileai.hotweb.domain.core;

import java.io.Serializable;
import java.security.Principal;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.agileai.hotweb.bizmoduler.frame.OnlineCounterService;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.Constants;
import com.agileai.util.StringUtil;

public class Profile implements Principal,Serializable,HttpSessionBindingListener{
	private static final long serialVersionUID = -8680004451151442270L;
	public static final String PROFILE_KEY = "__HotwebProfile__";
	public static final String CrossSessionIDKey = "__CrossSessionID__";
	public static final String CrossSessionMapKey = "__CrossSessionMap__";
	
	protected String userId = null;
	protected String fromIpAddress = null;
	protected Object user = null;
	
	protected HashMap<String,Object> attributes = new HashMap<String,Object>();
	
	public Profile(String userId,String fromIpAddress,Object user){
		this.userId = userId;
		this.fromIpAddress = fromIpAddress;
		this.user = user;
	}
	
	public Object getUser(){
		return user; 
	}
	
	public void valueBound(HttpSessionBindingEvent event) {
		HttpSession session = event.getSession();
		ServletContext application = session.getServletContext();
		String beanId = application.getInitParameter(Constants.OnlineCounterServiceKey);
		if (!StringUtil.isNullOrEmpty(beanId)){
			OnlineUserList onlineUserList = (OnlineUserList)application.getAttribute(OnlineUserList.AttributeKey);
			OnlineCounterService onlineCounterService = (OnlineCounterService)BeanFactory.instance().getBean(beanId);
			if (onlineUserList== null){
				onlineUserList = new OnlineUserList();
				application.setAttribute(OnlineUserList.AttributeKey, onlineUserList);
			}
			onlineUserList.add(this.userId);
			onlineCounterService.addOnlineCount();			
		}
	}

	@SuppressWarnings("unchecked")
	public void valueUnbound(HttpSessionBindingEvent event) {
		HttpSession session = event.getSession();
		String sessionId = session.getId();
		ServletContext application = session.getServletContext();
		String beanId = application.getInitParameter(Constants.OnlineCounterServiceKey);
		if (!StringUtil.isNullOrEmpty(beanId)){
			OnlineUserList onlineUserList = (OnlineUserList)application.getAttribute(OnlineUserList.AttributeKey);
			OnlineCounterService onlineCounterService = (OnlineCounterService)BeanFactory.instance().getBean(beanId);
			if (onlineUserList != null){
				onlineUserList.remove(this.userId);
				onlineCounterService.minusOnlineCount();
			}			
		}
		
		HashMap<String,HttpSession> sessionMap = (HashMap<String,HttpSession>)application.getAttribute(CrossSessionMapKey);
		if(sessionMap != null && sessionMap.containsKey(sessionId)) {
			sessionMap.remove(sessionId);
		}
	}

	public String getFromIpAddress() {
		return fromIpAddress;
	}
	public String getUserId() {
		return userId;
	}
	
	public Object getAttribute(String key){
		return this.attributes.get(key);
	}
	
	public void setAttribute(String key,Object value){
		this.attributes.put(key,value);
	}

	public String getName() {
		return this.userId;
	}
}
