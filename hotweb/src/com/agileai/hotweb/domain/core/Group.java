package com.agileai.hotweb.domain.core;

import java.io.Serializable;
import java.util.HashMap;

public class Group implements Serializable{
	private static final long serialVersionUID = -8288375741790285328L;
	
	private String groupId = null;
	private String groupCode = null;
	private String groupName = null;
	
	private HashMap<String,Object> extendProperties = new HashMap<String,Object>();
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public HashMap<String, Object> getExtendProperties() {
		return extendProperties;
	}
}
